--
-- PostgreSQL database dump
--

-- Dumped from database version 16.2
-- Dumped by pg_dump version 16.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: hub_ort; Type: TABLE DATA; Schema: adressen; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE adressen.hub_ort DISABLE TRIGGER ALL;

COPY adressen.hub_ort (ort_id, ortsname, parentort, iso_3166_1, iso_3166_2) FROM stdin;
1	Berlin	\N	DE	BE
2	Biberach	\N	DE	BW
3	Bremen	\N	DE	HB
4	Bübingen	\N	DE	SL
5	Darmstadt	\N	DE	BW
6	Frankfurt	\N	DE	HE
7	Freiburg	\N	DE	BW
8	Garching	\N	DE	BY
9	Gräfelfing	\N	DE	BY
10	Kirchentellinsfurt	\N	DE	BW
11	Konstanz	\N	DE	BW
12	Kronach	\N	DE	BY
13	Leipzig	\N	DE	SN
14	Mandelbachtal	\N	DE	SL
15	München	\N	DE	BY
16	Northeim	\N	DE	NW
17	Pirmasens	\N	DE	RP
18	Regensburg	\N	DE	BY
19	Saarbrücken	\N	DE	SL
20	St. Ingbert	\N	DE	SL
21	Straßberg	\N	DE	BW
22	Stuttgart	\N	DE	BW
23	Tarmstedt	\N	DE	NI
24	Veitsbronn	\N	DE	BY
25	Weiden	\N	DE	BY
26	Weiler bei Bingen	\N	DE	RP
27	Wiesbaden	\N	DE	HE
28	Aeugst-am-Albis	\N	CH	ZH
29	Altnau	\N	CH	TG
30	Basel	\N	CH	BS
31	Dietikon	\N	CH	ZH
32	Dietlikon	\N	CH	ZH
33	Dübendorf	\N	CH	ZH
34	Engelberg	\N	CH	OW
35	Eschlikon	\N	CH	TG
36	Fiesch	\N	CH	VS
37	Horgen	\N	CH	ZH
38	Kreuzlingen	\N	CH	TG
39	Meilen	\N	CH	ZH
40	Niederhasli	\N	CH	ZH
41	Sisikon	\N	CH	UR
42	Suhr	\N	CH	AG
43	Thalwil	\N	CH	ZH
44	Zürich	\N	CH	ZH
45	Amsterdam	\N	NL	NH
46	Anerley	\N	GB	BRY
47	Aalborg	\N	DK	81
48	Bogotá	\N	CO	DC
49	Capanne	\N	IT	52
50	Cascina	\N	IT	52
51	Cervià de Ter	\N	ES	GI
52	Chiang Mai	\N	TH	50
53	Columbus	\N	US	OH
54	Lavaino	\N	IT	52
55	Le Bardo (Tunis)	\N	TN	11
56	Leeds	\N	GB	WYK
57	Los Angeles	\N	US	CA
58	Massa	\N	IT	52
59	Medellín	\N	CO	ANT
60	Moskau	\N	RU	MOW
61	Paris	\N	FR	75
62	Pudsey	\N	GB	WYK
63	São Paulo	\N	BR	SP
64	Sarreguemines	\N	FR	57
65	Skopje	\N	MK	85
66	Split	\N	HR	17
67	Stalybridge	\N	GB	TAM
68	Udine	\N	IT	36
69	Venedig	\N	IT	34
70	Verona	\N	IT	34
71	Vigo	\N	ES	PO
72	Villa de Leyva	\N	CO	BOY
73	West Palm Beach	\N	US	FL
74	Weyersheim	\N	FR	67
75	Thessaloniki	\N	GR	B
76	Ensheim	19	\N	\N
77	Eschringen	19	\N	\N
78	Sankt Arnual	19	\N	\N
79	Ormesheim	14	\N	\N
80	Fiescheralp	36	\N	\N
81	Affoltern	44	\N	\N
82	Albisrieden	44	\N	\N
83	Altstadt	44	\N	\N
84	Aussersihl	44	\N	\N
85	Enge	44	\N	\N
86	Fluntern	44	\N	\N
87	Höngg	44	\N	\N
88	Hottingen	44	\N	\N
89	Industriequartier	44	\N	\N
90	Leimbach	44	\N	\N
91	Riesbach	44	\N	\N
92	Schwamendingen	44	\N	\N
93	Seebach	44	\N	\N
94	Unterstrass	44	\N	\N
95	Wiedikon	44	\N	\N
96	Wipkingen	44	\N	\N
97	Wollishofen	44	\N	\N
98	Pankow	1	\N	\N
99	Prenzlauer Berg	98	\N	\N
100	Friedrichshain-Kreuzberg	1	\N	\N
101	Kreuzberg	100	\N	\N
102	Antonio Nariño	48	\N	\N
103	Barrios Unidos	48	\N	\N
104	Bosa	48	\N	\N
105	Chapinero	48	\N	\N
106	Ciudad Bolívar	48	\N	\N
107	Engativá	48	\N	\N
108	Fontibón	48	\N	\N
109	Kennedy	48	\N	\N
110	La Candelaria	48	\N	\N
111	Los Mártires	48	\N	\N
112	Puente Aranda	48	\N	\N
113	Rafael Uribe Uribe	48	\N	\N
114	San Cristóbal	48	\N	\N
115	Santa Fe	48	\N	\N
116	Sumapaz	48	\N	\N
117	Teusaquillo	48	\N	\N
118	Tunjuelito	48	\N	\N
119	Usaquén	48	\N	\N
120	Usme	48	\N	\N
121	Suba	48	\N	\N
122	La Campiña de Suba	121	\N	\N
123	Buchs	\N	CH	ZH
124	Edinburg	\N	US	TX
125	Oberstrass	44	\N	\N
126	Adliswil	\N	CH	ZH
127	Kilchberg	\N	CH	ZH
128	Staad	\N	CH	SG
129	Lindau	\N	DE	BY
130	Mailand	\N	IT	25
131	Stadland	\N	DE	NI
133	Rodenkirchen	131	\N	\N
134	Cuxhaven	\N	DE	NI
135	Nienburg/Weser	\N	DE	NI
136	Hamburg	\N	DE	HH
137	Trendelburg	\N	DE	NW
138	Gottsbüren	137	\N	\N
139	Scuol	\N	CH	GR
140	Zweibrücken	\N	DE	RP
141	Hoya	\N	DE	NI
142	Einsiedeln	\N	CH	SZ
143	Molsheim	\N	FR	68
144	Saint-Emilion	\N	FR	33
145	Preignac	\N	FR	33
146	Epernay	\N	FR	51
147	Peso da Régua	\N	PT	17
148	Vila Nova de Gaia	\N	PT	13
149	Aleksandrovac	\N	RS	19
150	Exo Gonia	\N	GR	L
154	Perivolia	150	\N	\N
155	Perl	\N	DE	SL
156	Nennig	\N	DE	SL
157	Brackenheim	\N	DE	BW
158	Bern	\N	CH	BE
159	Spreitenbach	\N	CH	AG
160	Prémanon	\N	FR	39
161	Gibswil	\N	CH	ZH
162	Heilbronn	\N	DE	BW
163	Ebikon	\N	CH	LU
164	Wallisellen	\N	CH	ZH
165	Altstetten	44	\N	\N
166	Birkweiler	\N	DE	RP
167	Oerlikon	44	\N	\N
168	Augsburg	\N	DE	BY
169	Witikon	44	\N	\N
170	Landquart	\N	CH	GR
171	Boudry	\N	CH	NE
172	Gioia del Colle	\N	IT	75
173	Sankt Gallen	\N	CH	SG
174	Vo'	\N	IT	34
175	Castiglione Tinella	\N	IT	21
176	Vertus	\N	FR	51
177	Lauerz	\N	CH	SZ
178	Maur	\N	CH	ZH
179	Wasserauen	\N	CH	AI
180	Bar	\N	ME	02
181	Sutomore	\N	ME	02
182	Virpazar	\N	ME	02
183	Kotor	\N	ME	10
184	Podgorica	\N	ME	16
185	Como	\N	IT	25
186	Genua	\N	IT	42
187	Dresden	\N	DE	SN
188	Jestetten	\N	DE	BW
189	Rheinfelden	\N	DE	BW
190	Erfurt	\N	DE	TH
191	Lavertezzo	\N	CH	TI
192	La Chaux-de-Fonds	\N	CH	NE
193	Straßburg	\N	FR	67
194	Elm	\N	CH	GR
195	Barcelona	\N	ES	B
197	Morschach	\N	CH	SZ
198	Stoos	197	\N	\N
199	Brunnen	\N	CH	SZ
200	Flüelen	\N	CH	UR
201	Bellinzona	\N	CH	TI
202	Vicosoprano	\N	CH	GR
203	Laufen-Uhwiesen	\N	CH	ZH
204	Stein am Rhein	\N	CH	SH
205	Fribourg	\N	CH	FR
206	Gruyères	\N	CH	FR
\.


ALTER TABLE adressen.hub_ort ENABLE TRIGGER ALL;

--
-- Name: hub_ort_ort_id_seq; Type: SEQUENCE SET; Schema: adressen; Owner: -
--

SELECT pg_catalog.setval('adressen.hub_ort_ort_id_seq', 206, true);


--
-- PostgreSQL database dump complete
--

