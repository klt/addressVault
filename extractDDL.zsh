#!/usr/bin/env zsh
set -euo pipefail
DB=$1
TABLES_TO_EXTRACT=("hub_land" "hub_region" "hub_ort")

function dumpTableData {
    local tabName=$1
    local tabNr=$2
    local fullTabName="adressen.${tabName}"
    local outFile="${tabNr}_${tabName}.sql"

    echo "Extracting data from table ${fullTabName} into file ${outFile}"
    pg_dump -f ${outFile} -t ${fullTabName} --no-owner --no-privileges --data-only --disable-triggers ${DB}
}

echo "Extracting DDL from database ${DB} schema adressen into file 1_adressen.sql"
pg_dump -f 1_adressen.sql -n adressen --no-owner --no-privileges  --schema-only ${DB}

i=2
for tab in ${TABLES_TO_EXTRACT}; do
    dumpTableData ${tab} $((i))
    i=$((1+i))
done
