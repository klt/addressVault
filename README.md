# addressVault
A data vault like db schema to hold your addresses. The schema contains functions to export to bbdb, vCard and a simple LaTeX birthday list.

## Dependency
Created and developped under [PostgreSQL 12](https://www.postgresql.org "PostgreSQL"). May work for older version as well.
Needs a database without schema **adressen**.

## Installation
Execute the following:
``` shell
psql -d <db> -f 0_dbPreparation.sql -f 1_adressen.sql -f 2_hub_land.sql -f 3_hub_region.sql -f 4_hub_ort.sql

psql -d <db> -c "INSERT INTO adressen.hub_gruppe (gruppen_name, kommentar) VALUES('Geburtstag', 'members of this group will appear in view geburtstag');"
```
