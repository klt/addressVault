--
-- PostgreSQL database dump
--

-- Dumped from database version 16.2
-- Dumped by pg_dump version 16.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: hub_land; Type: TABLE DATA; Schema: adressen; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE adressen.hub_land DISABLE TRIGGER ALL;

COPY adressen.hub_land (iso_3166_1, laendername, laendername2) FROM stdin;
AF	Afghanistan	\N
EG	Ägypten	\N
AX	Åland	\N
AL	Albanien	\N
DZ	Algerien	\N
AS	Amerikanisch-Samoa	\N
VI	Amerikanische Jungferninseln	\N
AD	Andorra	\N
AO	Angola	\N
AI	Anguilla	\N
AQ	Antarktika	\N
AG	Antigua und Barbuda	\N
GQ	Äquatorialguinea	\N
AR	Argentinien	\N
AM	Armenien	\N
AW	Aruba	\N
AC	Ascension	\N
AZ	Aserbaidschan	\N
ET	Äthiopien	\N
AU	Australien	\N
BS	Bahamas	\N
BH	Bahrain	\N
BD	Bangladesch	\N
BB	Barbados	\N
BY	Belarus	\N
BE	Belgien	\N
BZ	Belize	\N
BJ	Benin	\N
BM	Bermuda	\N
BT	Bhutan	\N
BO	Bolivien	\N
BQ	Bonaire, Sint Eustatius und Saba	\N
BA	Bosnien und Herzegowina	\N
BW	Botswana	\N
BV	Bouvetinsel	\N
BR	Brasilien	\N
VG	Britische Jungferninseln	\N
IO	Britisches Territorium im Indischen Ozean	\N
BN	Brunei Darussalam	\N
BG	Bulgarien	\N
BF	Burkina Faso	\N
BI	Burundi	\N
EA	Ceuta Melilla	\N
CL	Chile	\N
CN	China	\N
CP	Clipperton	\N
CK	Cookinseln	\N
CR	Costa Rica	\N
CI	Elfenbeinküste	\N
CW	Curaçao	\N
DK	Dänemark	\N
DE	Deutschland	\N
DG	Diego Garcia	\N
DM	Dominica	\N
DO	Dominikanische Republik	\N
DJ	Dschibuti	\N
EC	Ecuador	\N
SV	El Salvador	\N
ER	Eritrea	\N
EE	Estland	\N
FK	Falklandinseln	\N
FO	Färöer	\N
FJ	Fidschi	\N
FI	Finnland	\N
FR	Frankreich	\N
GF	Französisch-Guayana	\N
PF	Französisch-Polynesien	\N
TF	Französische Süd- und Antarktisgebiete	\N
GA	Gabun	\N
GM	Gambia	\N
GE	Georgien	\N
GH	Ghana	\N
GI	Gibraltar	\N
GD	Grenada	\N
GR	Griechenland	\N
GL	Grönland	\N
GP	Guadeloupe	\N
GU	Guam	\N
GT	Guatemala	\N
GG	Guernsey	\N
GN	Guinea	\N
GW	Guinea-Bissau	\N
GY	Guyana	\N
HT	Haiti	\N
HM	Heard und McDonaldinseln	\N
HN	Honduras	\N
HK	Hongkong	\N
IN	Indien	\N
ID	Indonesien	\N
IM	Insel Man	\N
IQ	Irak	\N
IR	Iran	\N
IE	Irland	\N
IS	Island	\N
IL	Israel	\N
JM	Jamaika	\N
JP	Japan	\N
YE	Jemen	\N
JE	Jersey	\N
JO	Jordanien	\N
KY	Kaimaninseln	\N
KH	Kambodscha	\N
CM	Kamerun	\N
CA	Kanada	\N
IC	Kanarische Inseln	\N
CV	Kap Verde	\N
KZ	Kasachstan	\N
QA	Katar	\N
KE	Kenia	\N
KG	Kirgisistan	\N
KI	Kiribati	\N
CC	Kokosinseln	\N
CO	Kolumbien	\N
KM	Komoren	\N
CD	Demokratische Republik Kongo	\N
CG	Republik Kongo	\N
KP	Nordkorea	\N
KR	Südkorea	\N
XK	Kosovo	\N
HR	Kroatien	\N
CU	Kuba	\N
KW	Kuwait	\N
LA	Laos	\N
LS	Lesotho	\N
LV	Lettland	\N
LB	Libanon	\N
LR	Liberia	\N
LY	Libyen	\N
LI	Liechtenstein	\N
LT	Litauen	\N
LU	Luxemburg	\N
MO	Macau	\N
MG	Madagaskar	\N
MW	Malawi	\N
MY	Malaysia	\N
MV	Malediven	\N
ML	Mali	\N
MT	Malta	\N
MA	Marokko	\N
MH	Marshallinseln	\N
MQ	Martinique	\N
MR	Mauretanien	\N
MU	Mauritius	\N
YT	Mayotte	\N
MK	Mazedonien	\N
MX	Mexiko	\N
FM	Mikronesien	\N
MD	Moldawien	\N
MC	Monaco	\N
MN	Mongolei	\N
ME	Montenegro	\N
MS	Montserrat	\N
MZ	Mosambik	\N
NA	Namibia	\N
NR	Nauru	\N
NP	Nepal	\N
NC	Neukaledonien	\N
NZ	Neuseeland	\N
NI	Nicaragua	\N
NL	Niederlande	\N
NE	Niger	\N
NG	Nigeria	\N
NU	Niue	\N
MP	Nördliche Marianen	\N
NF	Norfolkinsel	\N
NO	Norwegen	\N
OM	Oman	\N
AT	Österreich	\N
TL	Osttimor	\N
PK	Pakistan	\N
PS	Palästina	\N
PW	Palau	\N
PA	Panama	\N
PG	Papua-Neuguinea	\N
PY	Paraguay	\N
PE	Peru	\N
PH	Philippinen	\N
PN	Pitcairninseln	\N
PL	Polen	\N
PT	Portugal	\N
PR	Puerto Rico	\N
TW	Taiwan	\N
RE	Réunion	\N
RW	Ruanda	\N
RO	Rumänien	\N
RU	Russische Föderation	\N
BL	Saint-Barthélemy	\N
MF	Saint-Martin	\N
SB	Salomonen	\N
ZM	Sambia	\N
WS	Samoa	\N
SM	San Marino	\N
ST	São Tomé und Príncipe	\N
SA	Saudi-Arabien	\N
SE	Schweden	\N
CH	Schweiz	\N
SN	Senegal	\N
RS	Serbien	\N
SC	Seychellen	\N
SL	Sierra Leone	\N
ZW	Simbabwe	\N
SG	Singapur	\N
SX	Sint Maarten	\N
SK	Slowakei	\N
SI	Slowenien	\N
SO	Somalia	\N
ES	Spanien	\N
LK	Sri Lanka	\N
SH	St. Helena	\N
KN	St. Kitts und Nevis	\N
LC	St. Lucia	\N
PM	Saint-Pierre und Miquelon	\N
VC	St. Vincent und die Grenadinen	\N
ZA	Südafrika	\N
SD	Sudan	\N
GS	Südgeorgien und die Südlichen Sandwichinseln	\N
SS	Südsudan	\N
SR	Suriname	\N
SJ	Svalbard und Jan Mayen	\N
SZ	Swasiland	\N
SY	Syrien	\N
TJ	Tadschikistan	\N
TZ	Tansania	\N
TH	Thailand	\N
TG	Togo	\N
TK	Tokelau	\N
TO	Tonga	\N
TT	Trinidad und Tobago	\N
TA	Tristan da Cunha	\N
TD	Tschad	\N
CZ	Tschechien	\N
TN	Tunesien	\N
TR	Türkei	\N
TM	Turkmenistan	\N
TC	Turks- und Caicosinseln	\N
TV	Tuvalu	\N
UG	Uganda	\N
UA	Ukraine	\N
HU	Ungarn	\N
UM	United States Minor Outlying Islands	\N
UY	Uruguay	\N
UZ	Usbekistan	\N
VU	Vanuatu	\N
VA	Vatikanstadt	\N
VE	Venezuela	\N
AE	Vereinigte Arabische Emirate	\N
US	Vereinigte Staaten von Amerika	\N
GB	Vereinigtes Königreich Großbritannien und Nordirland	\N
VN	Vietnam	\N
WF	Wallis und Futuna	\N
CX	Weihnachtsinsel	\N
EH	Westsahara	\N
CF	Zentralafrikanische Republik	\N
CY	Zypern	\N
IT	Italien	Italia
\.


ALTER TABLE adressen.hub_land ENABLE TRIGGER ALL;

--
-- PostgreSQL database dump complete
--

