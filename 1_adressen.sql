--
-- PostgreSQL database dump
--

-- Dumped from database version 16.2
-- Dumped by pg_dump version 16.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: adressen; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA adressen;


--
-- Name: cleanedishome; Type: TYPE; Schema: adressen; Owner: -
--

CREATE TYPE adressen.cleanedishome AS (
	cleaned text,
	is_home boolean
);


--
-- Name: emailwithalias; Type: TYPE; Schema: adressen; Owner: -
--

CREATE TYPE adressen.emailwithalias AS (
	epost character varying(64),
	alias character varying(8)
);


--
-- Name: typedadress; Type: TYPE; Schema: adressen; Owner: -
--

CREATE TYPE adressen.typedadress AS (
	typ text,
	strasse text,
	zusatz text,
	postfach text,
	ortsname character varying,
	iso_3166_2 character varying,
	plz character varying,
	laendername character varying
);


--
-- Name: typedphonenr; Type: TYPE; Schema: adressen; Owner: -
--

CREATE TYPE adressen.typedphonenr AS (
	typ text,
	telnr bigint
);


--
-- Name: vcard_type; Type: TYPE; Schema: adressen; Owner: -
--

CREATE TYPE adressen.vcard_type AS (
	id smallint,
	fn text,
	n text,
	nickname text,
	bday character(8),
	url text,
	adr text,
	tel text,
	email text,
	categories text,
	note text,
	photo text
);


--
-- Name: bbdbline(smallint, text, text, character varying, text, character varying, character varying, bigint); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.bbdbline(pfid smallint, vorname text, nachname text, spitzname character varying, telnummern text, epost character varying, mail_alias character varying, rnk bigint) RETURNS TABLE(id smallint, bbdb_text text)
    LANGUAGE plpgsql
    AS $$
Declare
  declare bbdb_mail_alias constant text = coalesce(('((mail-alias . "' || mail_alias) || '"))', 'nil') ;
  declare bbdb_spitzname constant text = coalesce('("' || spitzname || '")', 'nil') ; 
  declare bbdb_name constant text = '["' || vorname || '" "' || nachname || '" nil ' || bbdb_spitzname || ' nil ' ;
  declare tempus constant text = to_char(current_timestamp, 'yyyy-mm-dd hh24:mi:ss') ;
Begin
RETURN QUERY
 select pfid
 ,((((((((((((( bbdb_name ||
    telnummern) || ' nil ("') ||
    epost) || '") ') ||
    bbdb_mail_alias) || ' "pid-') ||
    pfid) || '-') ||
    rnk) || '" "') ||
    tempus) || '" "') || tempus) || '" nil]'
 ;
End;
$$;


--
-- Name: createsingletextarg(text); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.createsingletextarg(str text) RETURNS text
    LANGUAGE sql
    AS $$
  select adressen.createTextArg(str => '{'||trim(str)||'}') ;
$$;


--
-- Name: createtextarg(text); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.createtextarg(str text) RETURNS text
    LANGUAGE sql
    AS $$
  select coalesce(''''||trim(str)||'''','NULL');
$$;


--
-- Name: delete_firma(integer[]); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.delete_firma(fidarray integer[]) RETURNS TABLE(firma_id smallint, firmenname character varying)
    LANGUAGE sql
    AS $$
  delete from adressen.sat_firma_epost where firma_id = any(fidArray) ;
  delete from sal_firma_ort
  where lnk_fir_ort_id in (select lnk_fir_ort_id
                           from   adressen.lnk_firma_ort
                           where  firma_id = any(fidArray)) ;
  delete from adressen.lnk_firma_gruppe  where firma_id = any(fidArray) ;
  delete from adressen.lnk_firma_ort     where firma_id = any(fidArray) ;
  delete from adressen.lnk_firma_telefon where firma_id = any(fidArray) ;
  delete from adressen.hub_firma         where firma_id = any(fidArray) returning firma_id,firmenname ;
$$;


--
-- Name: delete_firma(integer); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.delete_firma(fid integer) RETURNS TABLE(firma_id smallint, firmenname character varying)
    LANGUAGE sql
    AS $$
  select * from delete_firma(array[fid]);
$$;


--
-- Name: delete_person(integer[]); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.delete_person(pidarray integer[]) RETURNS TABLE(person_id smallint, spitzname character varying, geburtsdatum date, notiz text)
    LANGUAGE sql
    AS $$
  delete from adressen.sat_person_bild  where person_id = any(pidArray) ;
  delete from adressen.sat_person_epost where person_id = any(pidArray) ;
  delete from adressen.sat_person_namen where person_id = any(pidArray) ;
  delete from adressen.sal_person_ort
  where lnk_per_ort_id in (select lnk_per_ort_id
                           from   adressen.lnk_person_ort
                           where  person_id = any(pidArray)) ;
  delete from adressen.lnk_person_gruppe  where person_id = any(pidArray) ;
  delete from adressen.lnk_person_ort     where person_id = any(pidArray) ;
  delete from adressen.lnk_person_telefon where person_id = any(pidArray) ;
  delete from adressen.hub_person         where person_id = any(pidArray) returning * ;
$$;


--
-- Name: delete_person(integer); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.delete_person(pid integer) RETURNS TABLE(person_id smallint, spitzname character varying, geburtsdatum date, notiz text)
    LANGUAGE sql
    AS $$
  select * from adressen.delete_person(array[pid]);
$$;


--
-- Name: delete_person(character varying); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.delete_person(nom character varying) RETURNS TABLE(person_id smallint, spitzname character varying, geburtsdatum date, notiz text)
    LANGUAGE plpgsql
    AS $$
  declare pids integer[] ;
Begin
  SET CLIENT_ENCODING TO 'UTF8' ;
  SET search_path TO adressen,public;
  select array_agg(adressen.sat_person_namen.person_id)
  from   adressen.sat_person_namen
  where  adressen.sat_person_namen.nomen=nom
  into   pids
  ;
  return query select * from adressen.delete_person(pids);
End ;
$$;


--
-- Name: duplicateperson(smallint, text[], text[], date, text[], text); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.duplicateperson(pid smallint, vornamen text[], nachnamen text[], gebdatum date DEFAULT NULL::date, epostadressen text[] DEFAULT NULL::text[], nota text DEFAULT NULL::text) RETURNS smallint
    LANGUAGE plpgsql
    AS $$
  declare tempus        constant date = current_date ;
  declare newPid        hub_person.person_id%TYPE ;
  declare lnkPerOrtId   lnk_person_ort.lnk_per_ort_id%TYPE ;

  declare epostAdresse  text ;
Begin
--- insert into hub_personen
  insert into hub_person(Geburtsdatum,Notiz) values (Gebdatum,Nota) returning person_id into newPid ;
    if (Vornamen is not null)
  then begin
    for i in 1 .. coalesce(array_upper(Vornamen, 1),0)
    loop
      insert into sat_person_namen(person_id,namen_position,nomen) values (newpid,i,Vornamen[i]) ;
    end loop;
  end ;
  end if ;
--- insert into sat_person_namen: Nachnamen
  if (Nachnamen is not null)
  then begin
    for i in 1 .. coalesce(array_upper(Nachnamen, 1),0)
    loop
      insert into sat_person_namen(person_id,namen_position,nomen) values (newpid,0-i,Nachnamen[i]) ;
    end loop;
  end ;
  end if ;
  return newPid ;
--- inserting email
  if (Epostadressen is not null)
  then begin
    update sat_person_epost set valid_to = tempus where person_id = newpid ;
    for i in 1 .. coalesce(array_upper(EpostAdressen, 1),0)
    loop
      epostAdresse = trim(leading '^' from Epostadressen[i]) ;
      if (epostAdresse != '')
      then begin
        insert into sat_person_epost(person_id,valid_from,epost,epost_is_home)
        values (newpid,tempus,epostAdresse,(epostAdresse=Epostadressen[i])) ;
      end ;
      end if ;
    end loop;
  end ;
  end if ;
--- copying values
  insert into lnk_person_gruppe(person_id,gruppen_id)
         select newPid,gruppen_id from lnk_person_gruppe where person_id=pid ;
  insert into lnk_person_telefon(person_id,telefon,valid_from)
         select newPid,telefon,tempus from lnk_person_telefon where person_id=pid and valid_to is not null ;
  insert into lnk_person_ort(person_id,ort_id)
         select newPid,ort_id from lnk_person_ort where person_id=pid ;
  insert into sal_person_ort(lnk_per_ort_id,valid_from,postfach,zusatz,strasse,plz,ort_is_home)
         select l_new.lnk_per_ort_id,tempus,sal.postfach,sal.zusatz,sal.strasse,sal.plz,sal.ort_is_home
         from   sal_person_ort            sal
                inner join lnk_person_ort l_old on sal.lnk_per_ort_id = l_old.lnk_per_ort_id
                inner join lnk_person_ort l_new on l_old.ort_id = l_new.ort_id
         where  sal.valid_to is not null
         and    l_old.person_id = pid
         and    l_new.person_id = newPid
         ;
End;
$$;


--
-- Name: duplicateperson(integer, text[], text[], date, text[], text); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.duplicateperson(pid integer, vornamen text[], nachnamen text[], gebdatum date DEFAULT NULL::date, epostadressen text[] DEFAULT NULL::text[], nota text DEFAULT NULL::text) RETURNS smallint
    LANGUAGE plpgsql
    AS $$
  declare tempus        constant date = current_date ;
  declare newPid        smallint ;
  declare lnkPerOrtId   smallint ;
  declare epostAdresse  text ;
Begin
    SET CLIENT_ENCODING TO 'UTF8' ;
    SET search_path TO adressen,public;
--- insert into adressen.hub_personen
  insert into adressen.hub_person(Geburtsdatum,Notiz) values (Gebdatum,Nota) returning person_id into newPid ;
--- insert into adressen.sat_person_namen: Vornamen
  if (Vornamen is not null)
  then begin
       for i in 1 .. coalesce(array_upper(Vornamen, 1),0)
       loop
         insert into adressen.sat_person_namen(person_id,namen_position,nomen) values (newpid,i,Vornamen[i]) ;
       end loop;
       end ;
  end if ;
--- insert into adressen.sat_person_namen: Nachnamen
  if (Nachnamen is not null)
  then begin
       for i in 1 .. coalesce(array_upper(Nachnamen, 1),0)
       loop
         insert into adressen.sat_person_namen(person_id,namen_position,nomen) values (newpid,0-i,Nachnamen[i]) ;
       end loop;
       end ;
  end if ;
--- inserting email
  if (Epostadressen is not null)
  then begin
       update adressen.sat_person_epost set validity = daterange(lower(validity),tempus) where person_id = newpid ;
       for i in 1 .. coalesce(array_upper(EpostAdressen, 1),0)
       loop
         epostAdresse = trim(leading '^' from replace(Epostadressen[i],' ','')) ;
         if (epostAdresse != '')
         then begin
              insert into adressen.sat_person_epost(person_id,validity,epost,epost_is_home)
              values (newpid,daterange(tempus,null),epostAdresse,(epostAdresse=Epostadressen[i])) ;
              end ;
         end if ;
       end loop;
       end ;
  end if ;
--- copying values
  insert into adressen.lnk_person_gruppe(person_id,gruppen_id)
         select newPid,gruppen_id from lnk_person_gruppe where person_id=pid ;
  insert into adressen.lnk_person_telefon(person_id,telefon,validity)
         select newPid,telefon,daterange(lower(validity),tempus) from lnk_person_telefon where person_id=pid and validity @> current_date ;
  insert into adressen.lnk_person_ort(person_id,ort_id)
         select newPid,ort_id from lnk_person_ort where person_id=pid ;
  insert into adressen.sal_person_ort(lnk_per_ort_id,validity,postfach,zusatz,strasse,plz,ort_is_home)
         select l_new.lnk_per_ort_id,daterange(lower(validity),tempus),sal.postfach,sal.zusatz,sal.strasse,sal.plz,sal.ort_is_home
         from   sal_person_ort            sal
                inner join lnk_person_ort l_old on sal.lnk_per_ort_id = l_old.lnk_per_ort_id
                inner join lnk_person_ort l_new on l_old.ort_id = l_new.ort_id
         where  sal.validity @> current_date
         and    l_old.person_id = pid
         and    l_new.person_id = newPid ;
  return newPid ;
End;
$$;


--
-- Name: exportclaw(); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.exportclaw() RETURNS text
    LANGUAGE plpgsql
    AS $$
  declare lb constant char(20) = chr(13)||chr(10) ;
  declare claw text ;
begin
	with t as
	(select adressen.exportclaw(vorname,nachname,spitzname,geburtsdatum,epost,phone,address) claw_record
     from adressen.export_personen
     union
     select adressen.exportclaw(firmenname,NULL,NULL,NULL,epost,phone,address) claw_record
     from adressen.export_firmen
	)
    select string_agg(claw_record,lb) into claw from t;
 return '<?xml version="1.0" encoding="UTF-8" ?>'||lb||'<address-book name="Allgemeine Adressen" >'||lb||claw||lb||'</address-book>';
End;
$$;


--
-- Name: exportclaw(text, text, character varying, date, adressen.emailwithalias[], adressen.typedphonenr[], adressen.typedadress[]); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.exportclaw(vorname text, nachname text, spitzname character varying, gebdat date, eposts adressen.emailwithalias[], telnrs adressen.typedphonenr[], postadressen adressen.typedadress[]) RETURNS text
    LANGUAGE plpgsql
    AS $$
  declare lb constant char(20) = chr(13)||chr(10) ;
  declare vor constant text = coalesce(vorname,'') ;
  declare nach constant text = coalesce(nachname,'') ;
  declare spitz constant text = coalesce(spitzname,'') ;
  declare claw text ;
  declare adr adressen.typedadress;
  declare epost adressen.emailwithalias;
  declare tel adressen.typedphonenr;
  declare addresslist text = '' ;
  declare attributelist text = '' ;
  declare typ text ; 
begin
  if (eposts is not null)
  then
    foreach epost in array eposts
      loop
         addresslist := addresslist||'     <address uid="'||nextval('smallint_serial')||'" alias="'||coalesce(epost.alias,'')||'" email="'||coalesce(epost.epost,'')||'" remarks="" />'||lb ;
      end loop ;
  end if ;
  if (telnrs is not null)
  then
    foreach tel in array telnrs
      loop
	     typ := case tel.typ when 'home' then 'Telefon' when 'cell' then 'Mobiltelefon' when 'fax' then 'Fax' else 'Telefon (Büro)' end ; 
         attributelist := attributelist||'     <attribute uid="'||nextval('smallint_serial')||'"  name="'||typ||'">'||coalesce(tel.telnr,-1)||'</attribute>'||lb ;
      end loop ;
  end if ;
  if (gebdat is not null)
  then
    attributelist := attributelist||'     <attribute uid="'||nextval('smallint_serial')||'"  name="Geburtsdatum">'||to_char(gebdat, 'YYYY-MM-DD')||'</attribute>'||lb ;
  end if ;
  if (postadressen is not null)
  then
    foreach adr in array postadressen
      loop
	     typ := case adr.typ when 'home' then 'Adresse' else 'Geschäftsadresse' end ; 
         attributelist := attributelist||'     <attribute uid="'||nextval('smallint_serial')||'"  name="'||typ||'">'||
         coalesce(adr.strasse||' , ','')||coalesce(adr.zusatz||' , ','')||coalesce(adr.postfach||' , ','')||
         coalesce(adr.plz||' ','')||coalesce(adr.ortsname||' , ','')||coalesce(adr.iso_3166_2||' , ','')||coalesce(adr.laendername,'')||'</attribute>'||lb ;
      end loop ;
  end if ;
 select '  <person uid="'||nextval('smallint_serial')||'" first-name="'||vor||'" last-name="'||nach||'" nick-name="'||spitz||'" cn="'||vor||' '||nach||'">'||
   lb||'    <address-list>'||lb||addresslist||'    </address-list>'||
   lb||'    <attribute-list>'||lb||attributelist||'    </attribute-list>'||
   lb||' </person>'
 into claw ;
 return claw ;
End;
$$;


--
-- Name: exportvcard(boolean); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.exportvcard(fotos boolean DEFAULT false) RETURNS TABLE(vc text)
    LANGUAGE plpgsql
    AS $$
  declare fids integer[] ;
  declare pids integer[] ;
Begin
  SET CLIENT_ENCODING TO 'UTF8' ;
  SET search_path TO adressen,public;

  select array_agg(firma_id)
  from   adressen.hub_firma
  into   fids
  ;
  select array_agg(person_id)
  from   adressen.hub_person
  into   pids
  ;
  return query select * from exportVcard(fidArray=>fids,pidArray=>pids,mitphotos=>fotos);
End;
$$;


--
-- Name: exportvcard(character varying, boolean); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.exportvcard(gruppenname character varying, fotos boolean DEFAULT false) RETURNS TABLE(vc text)
    LANGUAGE plpgsql
    AS $$
  declare fids integer[] ;
  declare pids integer[] ;
Begin
  SET CLIENT_ENCODING TO 'UTF8' ;
  SET search_path TO adressen,public;

  select array_agg(firma_id)
  from   adressen.lnk_firma_gruppe
  where  gruppen_id in (select gruppen_id from adressen.hub_gruppe where gruppen_name = gruppenName)
  into   fids
  ;
  select array_agg(person_id)
  from   lnk_person_gruppe
  where  gruppen_id in (select gruppen_id from adressen.hub_gruppe where gruppen_name = gruppenName)
  into   pids
  ;
  return query select * from exportVcard(fidArray=>fids,pidArray=>pids,mitphotos=>fotos);
End;
$$;


--
-- Name: FUNCTION exportvcard(gruppenname character varying, fotos boolean); Type: COMMENT; Schema: adressen; Owner: -
--

COMMENT ON FUNCTION adressen.exportvcard(gruppenname character varying, fotos boolean) IS 'export command:
psql -tA -c "select adressen.exportVcard(gruppenname=>''<GROUP NAME>'',fotos=>TRUE)" --output=${vCardsFile}';


--
-- Name: exportvcard(integer[], integer[], boolean); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.exportvcard(fidarray integer[], pidarray integer[] DEFAULT ARRAY[]::integer[], mitphotos boolean DEFAULT false) RETURNS TABLE(vc text)
    LANGUAGE plpgsql
    AS $_$
  declare lb       constant char(20) = '||chr(13)||chr(10)||' ;
  declare vcBegin  constant char(66) = '''BEGIN:VCARD'''||lb||'''VERSION:3.0'''||lb ;
  declare vcEnd    constant char(31) = lb||'''END:VCARD''' ;
  declare noAttr   constant varchar(16)[] = array['adr','email','photo','tel'] ;
  declare resultat text[] ;
  declare felder   text ;
  declare stmt     text ;
Begin
  SET CLIENT_ENCODING TO 'UTF8' ;
---  set columns to 75 ;
  SET search_path TO adressen,public;
  raise notice 'exportVcard: mitphotos = %', mitphotos;
  raise notice 'exportVcard: Exporting % person vcards',array_length(pidArray,1) ;
  raise notice 'exportVcard: Exporting % company vcards',array_length(fidArray,1) ;
  select  string_agg(case when (attribute_name = any (noAttr))
                          then 'COALESCE('||attribute_name||','''||upper(attribute_name)||':'')'
                          else ''''||upper(attribute_name)||':''||'||'COALESCE('||attribute_name||','''')'
                          end
                    ,lb
                    order by ordinal_position
                    )
  into   felder
  from   information_schema.attributes
  where  udt_schema='adressen' and udt_name='vcard_type' and ordinal_position>1
  ;
  stmt = 'select '||vcBegin||felder||vcEnd||' from prepareVcardPerson(pidArray=>$1,mitphotos=>$2) union all select '||vcBegin||felder||vcEnd||' from prepareVcardFirma(fidArray=>$3)' ;
  raise notice 'exportVcard: stmt = %', stmt;
  return query execute stmt using pidArray,mitphotos,fidArray ;
End;
$_$;


--
-- Name: exportvcard(integer, boolean, boolean); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.exportvcard(id integer, fotos boolean DEFAULT true, firma boolean DEFAULT false) RETURNS TABLE(vc text)
    LANGUAGE plpgsql
    AS $$
  declare fids integer[] ;
  declare pids integer[] ;
Begin
  SET CLIENT_ENCODING TO 'UTF8' ;
  SET search_path TO adressen,public;
  if (firma)
  then
     pids = ARRAY[]::integer[];
     fids = ARRAY[id];
  else
     pids = ARRAY[id];
     fids = ARRAY[]::integer[];
  end if;
  raise notice 'exportVcard: fotos = %', fotos;
  raise notice 'exportVcard: Exporting % person vcards',array_length(pids,1) ;
  return query select * from exportVcard(fidArray=>fids,pidArray=>pids,mitphotos=>fotos);
End;
$$;


--
-- Name: exportvcard(text[], text[], date, boolean); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.exportvcard(vornamen text[], nachnamen text[], gebdatum date DEFAULT NULL::date, fotos boolean DEFAULT true) RETURNS TABLE(vc text)
    LANGUAGE plpgsql
    AS $$
declare
  pid constant integer = (select * from adressen.getPid(vornamen, nachnamen, gebdatum));
  fids constant integer[] = ARRAY[]::integer[] ;
  pids constant integer[] = ARRAY[pid] ;
Begin
  SET CLIENT_ENCODING TO 'UTF8' ;
  SET search_path TO adressen,public;
  raise notice 'exportVcard: fotos = %', fotos;
  raise notice 'exportVcard: Exporting % person vcards',array_length(pids,1) ;
  return query select * from exportVcard(fidArray=>fids,pidArray=>pids,mitphotos=>fotos);
End;
$$;


--
-- Name: getbbdbfield(text, boolean); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.getbbdbfield(str text, nilifnull boolean DEFAULT false) RETURNS text
    LANGUAGE sql
    AS $$
  select coalesce('"'||str||'" ',case when nilIfNull then 'nil ' else '' end) ;
$$;


--
-- Name: getfulladresse(boolean, integer[]); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.getfulladresse(person boolean DEFAULT true, idarray integer[] DEFAULT NULL::integer[]) RETURNS TABLE(id smallint, ort_is_home boolean, postfach text, zusatz text, strasse text, plz character varying, ort_id smallint, ortsname character varying, iso_3166_2 character varying, regionenname character varying, iso_3166_1 character, laendername character varying)
    LANGUAGE sql
    AS $$
  SET CLIENT_ENCODING TO 'UTF8' ;
  SET search_path TO adressen,public ;
  ;
  with lnk as
  (select person_id         id
         ,lnk_per_ort_id    linkId
         ,ort_id
   from   adressen.lnk_person_ort
   where  person
   and   (person_id = any(idArray) or idArray is null)
   union all
   select firma_id          id
         ,lnk_fir_ort_id    linkId
         ,ort_id
   from   adressen.lnk_firma_ort
   where  not person
   and   (firma_id = any(idArray) or idArray is null)
  )
  ,sal as
  (select lnk_per_ort_id    linkId
         ,ort_is_home
         ,Postfach
         ,Zusatz
         ,Strasse
         ,plz
   from   adressen.sal_person_ort
   where  person
   and    validity @> current_date
   union all
   select lnk_fir_ort_id    linkId
         ,cast(null as boolean) ort_is_home
         ,Postfach
         ,Zusatz
         ,Strasse
         ,plz
   from   adressen.sal_firma_ort
   where  not person
   and    validity @> current_date
  )
  select lnk.id
        ,sal.ort_is_home
        ,sal.Postfach
        ,sal.Zusatz
        ,sal.Strasse
        ,sal.plz
        ,ort.ort_id
        ,ort.ortsname
        ,ort.iso_3166_2
        ,ort.regionenname
        ,ort.iso_3166_1
        ,ort.laendername
  from   lnk inner join sal on lnk.linkId = sal.linkId
         inner join adressen.getFullOrtsnamen(oidArray=>(select array_agg(ort_id) from lnk)) ort
         on lnk.ort_id = ort.ort_id
  ;
$$;


--
-- Name: getfullinformation(boolean, integer[]); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.getfullinformation(person boolean DEFAULT true, idarray integer[] DEFAULT NULL::integer[]) RETURNS TABLE(id smallint, ort_is_home boolean, postfach text, zusatz text, strasse text, plz character varying, ort_id smallint, ortsname character varying, iso_3166_2 character varying, regionenname character varying, iso_3166_1 character, laendername character varying, telefon text, epost character varying, categories text)
    LANGUAGE sql
    AS $$
  SET CLIENT_ENCODING TO 'UTF8' ;
  SET search_path TO adressen,public ;
  with grpLnk as
  (select person_id         id
         ,gruppen_id
   from   adressen.lnk_person_gruppe
   where  person
   and   (person_id = any(idArray) or idArray is null)
   union all
   select firma_id          id
         ,gruppen_id
   from   adressen.lnk_firma_gruppe
   where  not person
   and   (firma_id = any(idArray) or idArray is null)
  )
  ,grp as
  (select grpLnk.id
         ,STRING_AGG(grp.gruppen_name,',' order by grp.gruppen_name)    CATEGORIES
   from   grpLnk inner join adressen.hub_gruppe grp
          on grpLnk.gruppen_id = grp.gruppen_id
   group by grpLnk.id
  )
  ,telLnk as
  (select person_id         id
         ,telefon
   from   adressen.lnk_person_telefon
   where  person
   and   (person_id = any(idArray) or idArray is null)
   and    validity @> current_date
   union all
   select firma_id          id
         ,telefon
   from   adressen.lnk_firma_telefon
   where  not person
   and   (firma_id = any(idArray) or idArray is null)
   and    validity @> current_date
  )
  ,tel as
  (select telLnk.id
         ,STRING_AGG('+'||tel.telefon,' ' order by tel.telefon)    Telefon
   from   telLnk inner join adressen.hub_telefon       tel
          on telLnk.telefon = tel.telefon
   group by telLnk.id
  )
  ,epo as
  (select person_id                                                 id
         ,STRING_AGG(epost,' ' order by epost)                      Epost
   from   adressen.sat_person_epost
   where  person
   and   (person_id = any(idArray) or idArray is null)
   and    validity @> current_date
   group by person_id
   union all
   select firma_id                                                  id
         ,STRING_AGG(epost,' ' order by epost)                      Epost
   from   adressen.sat_firma_epost
   where  not person
   and   (firma_id = any(idArray) or idArray is null)
   and    validity @> current_date
   group by firma_id
  )
  select coalesce(adr.id,tel.id,epo.id,grp.id)
        ,adr.ort_is_home
        ,adr.Postfach
        ,adr.Zusatz
        ,adr.Strasse
        ,adr.plz            PLZ
        ,adr.ort_id
        ,adr.ortsname       Ort
        ,adr.iso_3166_2
        ,adr.regionenname   Region
        ,adr.iso_3166_1
        ,adr.laendername    Land
        ,tel.Telefon
        ,epo.Epost
        ,grp.CATEGORIES
  from   adressen.getFullAdresse(person,idArray)        adr
         full outer join tel
         on adr.id = tel.id
         full outer join epo
         on coalesce(adr.id,tel.id) = epo.id
         full outer join grp
         on coalesce(adr.id,tel.id,epo.id) = grp.id
$$;


--
-- Name: getfullortsnamen(integer[]); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.getfullortsnamen(oidarray integer[] DEFAULT NULL::integer[]) RETURNS TABLE(ort_id smallint, ortsname character varying, iso_3166_2 character varying, regionenname character varying, iso_3166_1 character, laendername character varying)
    LANGUAGE sql
    AS $$
  SET CLIENT_ENCODING TO 'UTF8' ;
  SET search_path TO adressen,public ;
  ;
  with RECURSIVE orte(ort_id,ortsname,parentort,iso_3166_1,iso_3166_2,lev) as
  (select ort_id
         ,ortsname
         ,parentort
         ,iso_3166_1
         ,iso_3166_2
         ,0                             lev
   from   adressen.hub_ort
   where  ort_id = any(oidArray) or oidArray is null
   union  all
   select o.ort_id
         ,hub.ortsname
         ,hub.parentort
         ,hub.iso_3166_1
         ,hub.iso_3166_2
         ,o.lev+1                       lev
   from   orte                          o
   inner join adressen.hub_ort          hub
--- going up the hierarchy
         on   hub.ort_id = o.parentort
  )
  ,ortStadt as
  (select distinct ort_id
         ,last_value(ortsname) over (partition by ort_id
                                     order by lev
                                     range between unbounded preceding and unbounded following)     stadtname
         ,first_value(ortsname) over(partition by ort_id
                                       order by lev
                                       range between unbounded preceding and unbounded following)   ortsname
         ,last_value(iso_3166_1) over (partition by ort_id
                                     order by lev
                                     range between unbounded preceding and unbounded following)     iso_3166_1
         ,last_value(iso_3166_2) over (partition by ort_id
                                     order by lev
                                     range between unbounded preceding and unbounded following)     iso_3166_2
   from orte
  )
  select ort_id
        ,case when (stadtname=ortsname)
              then stadtname
              else stadtname||'-'||ortsname
         end ortsname
        ,reg.iso_3166_2
        ,reg.regionenname
        ,reg.iso_3166_1
        ,lnd.laendername
  from ortStadt
       inner join adressen.hub_region                         reg
               on ortStadt.iso_3166_1 = reg.iso_3166_1
              and ortStadt.iso_3166_2 = reg.iso_3166_2
       inner join adressen.hub_land                           lnd
               on reg.iso_3166_1 = lnd.iso_3166_1

  ;
$$;


--
-- Name: gethtmlfirma(character varying); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.gethtmlfirma(cat character varying) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
  lnd RECORD;
  htmlEintrag text;
  resultat text := '';
begin
  SET CLIENT_ENCODING TO 'UTF8' ;
  SET search_path TO adressen,public;
	for lnd in select distinct land from adressen.html_firma where categories like '%'||cat||'%' loop 
      select STRING_AGG(html,chr(10) order by region,firmenname)
      into   htmlEintrag
      from   adressen.html_firma
      where  categories like '%'||cat||'%'
      and    land=lnd.land
      ;
    resultat := resultat
                ||chr(10)||'<h2>'||lnd.land||'</h2>'
                ||chr(10)||'<dl>'
                ||chr(10)||htmlEintrag
                ||chr(10)||'</dl>'
                ;
  end loop;
  return resultat ;
End;
$$;


--
-- Name: getishome(text[]); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.getishome(inputarray text[]) RETURNS SETOF adressen.cleanedishome
    LANGUAGE sql
    AS $$
  SELECT trim(leading '^' from replace(intext,' ',''))
        ,intext=trim(leading '^' from replace(intext,' ',''))
  FROM unnest(inputarray) as t(intext)
$$;


--
-- Name: getnamen(integer[], integer, integer); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.getnamen(idarray integer[] DEFAULT NULL::integer[], maxvornamen integer DEFAULT 0, maxnachnamen integer DEFAULT 0) RETURNS TABLE(person_id smallint, vornamen text, nachnamen text)
    LANGUAGE sql
    AS $$
SET CLIENT_ENCODING TO 'UTF8' ;
SET search_path TO adressen,public ;
;
SELECT sat_person_namen.person_id,
    string_agg(
        CASE
            WHEN sat_person_namen.namen_position > 0 THEN sat_person_namen.nomen
            ELSE NULL::character varying
        END::text, ' '::text ORDER BY sat_person_namen.namen_position) AS vornamen,
    string_agg(
        CASE
            WHEN sat_person_namen.namen_position < 0 THEN sat_person_namen.nomen
            ELSE NULL::character varying
        END::text, ' '::text ORDER BY sat_person_namen.namen_position DESC) AS nachnamen
   FROM adressen.sat_person_namen
   where  (person_id = any(idArray) or idArray is null)
   and    (0=maxNachnamen or namen_position >= 0-maxNachnamen)
   and    (0=maxVornamen or namen_position <= maxVornamen)
  GROUP BY sat_person_namen.person_id
;
$$;


--
-- Name: getortid(text, character, character); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.getortid(stadt text DEFAULT NULL::text, countrycode character DEFAULT NULL::bpchar, regioncode character DEFAULT NULL::bpchar) RETURNS smallint
    LANGUAGE sql
    AS $$
  select ort_id
  from   adressen.hub_ort
  where  ortsname = stadt
  and    ort_id in (select ort_id from adressen.getregion(countryCode,regioncode))
  ;
$$;


--
-- Name: getpid(text[], text[], date); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.getpid(vornamen text[], nachnamen text[], gebdatum date DEFAULT NULL::date) RETURNS SETOF smallint
    LANGUAGE sql
    AS $$
  SET CLIENT_ENCODING TO 'UTF8' ;
  SET search_path TO adressen,public ;
  --- Person mit allen Vornamen
  select person_id
  from   adressen.sat_person_namen
  where  nomen = vornamen[namen_position]
  group  by person_id
  having count(*) = array_length(vornamen,1)
  intersect
  --- Person mit allen Nachnamen
  select person_id
  from   adressen.sat_person_namen
  where  nomen = nachnamen[0-namen_position]
  group  by person_id
  having count(*) = array_length(nachnamen,1)
  intersect
  --- Person mit dem Geburtsdatum
  select  person_id from adressen.hub_person where gebdatum is null or gebdatum=geburtsdatum
  ;
$$;


--
-- Name: getpid(text, text, date); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.getpid(vorname text, nachname text, gebdatum date DEFAULT NULL::date) RETURNS SETOF smallint
    LANGUAGE sql
    AS $$
  select * from adressen.getPid(ARRAY[vorname], ARRAY[nachname], gebdatum) ;
$$;


--
-- Name: getregion(character, character varying); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.getregion(countrycode character DEFAULT NULL::bpchar, regioncode character varying DEFAULT NULL::character varying) RETURNS TABLE(ort_id smallint, ortsname character varying, parentort smallint, iso_3166_1 character, iso_3166_2 character varying, niveau smallint)
    LANGUAGE sql
    AS $$
  SET CLIENT_ENCODING TO 'UTF8' ;
  SET search_path TO adressen,public ;
  with RECURSIVE orte(ort_id,ortsname,parentort,iso_3166_1,iso_3166_2,lev) as
  (select ort_id
         ,ortsname
         ,parentort
         ,iso_3166_1
         ,iso_3166_2
         ,0                     lev
   from   adressen.hub_ort
   where (iso_3166_1=countryCode or countryCode is null)
   and   (iso_3166_2=regionCode  or regionCode is null)
   union  all
   select hub.ort_id
         ,hub.ortsname
         ,hub.parentort
         ,hub.iso_3166_1
         ,hub.iso_3166_2
         ,o.lev+1                       lev
   from   orte                          o
   inner join adressen.hub_ort          hub
         on   hub.parentort = o.ort_id
  )
  select ort_id
        ,ortsname
        ,parentort
        ,iso_3166_1
        ,iso_3166_2
        ,cast(lev as smallint)
  from   orte
  ;
$$;


--
-- Name: insertneuefirma(text, text, text[], text[], text[], text[], text); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.insertneuefirma(firmname text, url text, telnummern text[] DEFAULT NULL::text[], epostadressen text[] DEFAULT NULL::text[], gruppen text[] DEFAULT NULL::text[], adressen text[] DEFAULT NULL::text[], nota text DEFAULT NULL::text) RETURNS TABLE(neue_id smallint, neuer_name character varying, neue_url character varying)
    LANGUAGE plpgsql
    AS $$
  declare tempus constant date = CURRENT_DATE ;
  declare isoCode       char(2) ;
  declare land          text ;
  declare lnkFirmaOrtId smallint ;
  declare ortId         smallint ;
  declare fid           smallint ;
  declare plz           text ;
  declare stadt         text ;
  declare strasse       text ;
  declare telNr         adressen.hub_telefon.telefon%TYPE ;
  declare telNrStr      text ;
  declare zusatz        text ;
Begin
  SET CLIENT_ENCODING TO 'UTF8' ;
  SET search_path TO adressen,public ;
--- insert into adressen.hub_firmaen
  insert into adressen.hub_firma(firmenname,firmen_url,Notiz) values (firmName,url,Nota) returning firma_id into fid ;
  insert into adressen.lnk_firma_gruppe(firma_id,gruppen_id)
         select fid,gruppen_id from adressen.hub_gruppe where gruppen_name = any (Gruppen) ;
--- inserting Telefonnummern
  if (Telnummern is not null)
  then perform adressen.link_tel(fid, Telnummern,false);
  end if ;
--- inserting email
  if (Epostadressen is not null)
  then perform adressen.link_email(fid, Epostadressen,false);
  end if ;
--- inserting Adressen
  if (Adressen is not null)
  then perform adressen.link_adresse(id => fid, adressen => adressen, isp => false);
  end if ;
  RETURN QUERY
  SELECT firma_id, firmenname, firmen_url FROM adressen.hub_firma where firma_id=fid
  ;
End;
$$;


--
-- Name: insertneueperson(text[], text[], date, text[], text[], text[], text[], text, text); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.insertneueperson(vornamen text[], nachnamen text[], gebdatum date DEFAULT NULL::date, telnummern text[] DEFAULT NULL::text[], epostadressen text[] DEFAULT NULL::text[], gruppen text[] DEFAULT NULL::text[], adressen text[] DEFAULT NULL::text[], nota text DEFAULT NULL::text, filename text DEFAULT NULL::text) RETURNS TABLE(neue_person_id smallint, rufnamen text, familiennamen text)
    LANGUAGE plpgsql
    AS $$
  declare isoCode       adressen.hub_land.iso_3166_1%TYPE ;
  declare isHome        boolean ;
  declare land          text ;
  declare lnkPersOrtId  smallint ;
  declare ortId         smallint ;
  declare pid           smallint ;
  declare plz           text ;
  declare stadt         text ;
  declare strasse       text ;
  declare zusatz        text ;
Begin
--- insert into adressen.hub_personen
  insert into adressen.hub_person(Geburtsdatum,Notiz) values (Gebdatum,Nota) returning person_id into pid ;
--- insert into adressen.lnk_person_gruppe_id
  insert into adressen.lnk_person_gruppe(person_id,gruppen_id)
         select pid,gruppen_id from adressen.hub_gruppe where gruppen_name = any (Gruppen) ;
--- insert into adressen.sat_person_namen: Vornamen
  if (Vornamen is not null)
  then begin
    for i in 1 .. coalesce(array_upper(Vornamen, 1),0)
    loop
      insert into adressen.sat_person_namen(person_id,namen_position,nomen) values (pid,i,Vornamen[i]) ;
    end loop;
  end ;
  end if ;
--- insert into adressen.sat_person_namen: Nachnamen
  if (Nachnamen is not null)
  then begin
    for i in 1 .. coalesce(array_upper(Nachnamen, 1),0)
    loop
      insert into adressen.sat_person_namen(person_id,namen_position,nomen) values (pid,0-i,Nachnamen[i]) ;
    end loop;
  end ;
  end if ;
--- inserting Telefonnummern
  if (Telnummern is not null)
  then perform adressen.link_tel(pid, Telnummern,true);
  end if ;
--- inserting email
  if (Epostadressen is not null)
  then perform adressen.link_email(pid, Epostadressen,true);
  end if ;
--- inserting Adressen
  if (Adressen is not null)
  then perform adressen.link_adresse(id => pid, adressen => adressen, isp => true);
  end if ;
--- inserting photo
  if (fileName is not null) then perform adressen.link_photo(pid,fileName) ;
  end if ;
  RETURN QUERY SELECT * from adressen.getnamen(array[pid]);
End;
$$;


--
-- Name: insertneuetelnr(text, boolean); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.insertneuetelnr(telnrstr text, firmatel boolean DEFAULT false) RETURNS bigint
    LANGUAGE plpgsql
    AS $$
  declare telNr text ;
  declare telNrDigitsOnly adressen.hub_telefon.telefon%TYPE ;
  declare telTyp adressen.hub_telefon.tel_typ%TYPE ;
  declare telIsHome adressen.hub_telefon.tel_is_home%TYPE ;
  declare telTypen constant text[] = Regexp_split_to_array(telNrStr, '\|') ;
Begin
  SET CLIENT_ENCODING TO 'UTF8' ;
  SET search_path TO adressen,public ;
    if (length(telTypen[1]) < 2) -- telTypen[1] == phone number
    then RAISE EXCEPTION 'insertneuetelnr: telnrstr = %', telnrstr
               USING HINT = 'insertneuetelnr: phone number must contain at least 2 digits. telnrstr  = '||telnrstr||' telTypen  = '||telTypen ;
    else begin
         telNr = trim(leading '^' from telTypen[1]) ;
         telIsHome = (cast(telNr as text)=telTypen[1]) ;
         telNrDigitsOnly = cast(regexp_replace(telNr,'[^[:digit:]]','','g') as bigint);
--- voice is default telTyp, i.e. when length(telTypen) == 1
         if (array_upper(telTypen, 1)=1) then telTyp = 'voice'; else telTyp = telTypen[2] ; end if ;
--- adding new phone numbers
         if (exists(select * from adressen.hub_telefon where telefon=telNrDigitsOnly))
         then raise notice 'insertneuetelnr: telNr % already exists in hub_telefon',telNrDigitsOnly ;
         else insert into adressen.hub_telefon
                          (telefon,tel_is_home,tel_typ)
                          values (telNrDigitsOnly,telIsHome and not firmaTel,telTyp) ;
         end if ;
         end ;
    end if ;
    return telNrDigitsOnly;
End;
$$;


--
-- Name: FUNCTION insertneuetelnr(telnrstr text, firmatel boolean); Type: COMMENT; Schema: adressen; Owner: -
--

COMMENT ON FUNCTION adressen.insertneuetelnr(telnrstr text, firmatel boolean) IS 'inserts new phone number into hub_telefon';


--
-- Name: link_adresse(smallint, text[], boolean); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.link_adresse(id smallint, adressen text[] DEFAULT NULL::text[], isp boolean DEFAULT true) RETURNS TABLE(pfid smallint, is_home boolean, pstfach text, zs text, street text, plzahl character varying, ortid smallint, ortsname character varying, iso_3166_2 character varying, regionenname character varying, iso_land character, land_name character varying)
    LANGUAGE plpgsql
    AS $$
declare
  declare isoCode       adressen.hub_land.iso_3166_1%TYPE ;
  declare isHome        boolean ;
  declare land          text ;
  declare plz           text ;
  declare stadt         text ;
  declare strasse       text ;
  declare postfach      text ;
  declare zusatz        text ;
Begin
  SET CLIENT_ENCODING TO 'UTF8' ;
  SET search_path TO adressen,public;

  if (Adressen is not null)
  then begin
    for i in 1 .. coalesce(array_upper(Adressen, 1),0)
    loop
      zusatz = unnest(Adressen[i:i][1:1]) ;
      postfach = unnest(Adressen[i:i][2:2]) ;
      strasse = unnest(Adressen[i:i][3:3]) ;
      plz = unnest(Adressen[i:i][4:4]) ;
      stadt = unnest(Adressen[i:i][5:5]) ;
      land = unnest(Adressen[i:i][6:6]) ;
      isoCode = case when (char_length(land)=2)
                     then land
                     else (select iso_3166_1 from adressen.hub_land where laendername=land)
                end ;
      isHome = unnest(Adressen[i:i][7:7]) ;
      if (char_length(stadt)>0 and char_length(land)>0)
      then begin
	       raise notice 'link_adresse: performing adressen.link_ort with id => %', id;
	       perform adressen.link_ort(id => id,
                                     valty => daterange(CURRENT_DATE, NULL::date),
                                     str => strasse,
                                     postzahl => plz,
                                     stadt => stadt,
                                     land => land,
                                     pfach => postfach,
                                     zus => zusatz,
                                     home => isHome,
                                     isperson => isp) ;
           end ;
       else raise notice 'link_adresse: stadt or land have length 0!';
       end if ;
    end loop;
  end ;
  end if ;
  RETURN QUERY select * from adressen.getfulladresse(isp,array[id]);
End ;
$$;


--
-- Name: link_email(smallint, text[], boolean); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.link_email(id smallint, epostadressen text[], isperson boolean DEFAULT true) RETURNS TABLE(idnr smallint, epostadresse character varying, email_is_home boolean, epost_alias character varying)
    LANGUAGE plpgsql
    AS $$
Declare
  emlLinked bool ;
  emlIsHome adressen.sat_person_epost.epost_is_home%TYPE ;
  eml       adressen.sat_person_epost.epost%TYPE  ;
  epostadr  text[] ;
Begin
  SET CLIENT_ENCODING TO 'UTF8' ;
  SET search_path TO adressen,public ;
  if (id is null)
  then RAISE EXCEPTION 'id is null !' ;
  else if isperson
       then -- linking epostAdressen to person via sat_person_epost
         insert into adressen.sat_person_epost(person_id,epost,epost_is_home)
         SELECT id,cleaned,is_home
         from   adressen.getishome(epostadressen)
         where  not exists(select * from adressen.sat_person_epost where person_id=id and epost=cleaned)
         ;
         RETURN QUERY
         select person_id, epost, epost_is_home, mail_alias
         from   adressen.sat_person_epost
         where  person_id=id and  validity @> current_date
         order by epost;
       else -- linking epostAdressen to Firma via sat_firma_epost
         insert into adressen.sat_firma_epost(firma_id,epost)
         SELECT id,cleaned
         from   adressen.getishome(epostadressen)
         where  not exists(select * from adressen.sat_firma_epost where firma_id=id and epost=cleaned)
         ;
         RETURN QUERY
         select firma_id,epost, false, cast(null as varchar(8))
         from   adressen.sat_firma_epost
         where  firma_id=id and  validity @> current_date
         order by epost;
       end if ;
  end if;
End; 
$$;


--
-- Name: link_ort(smallint, daterange, text, character varying, text, text, text, text, boolean, boolean); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.link_ort(id smallint, valty daterange, str text, postzahl character varying, stadt text, land text, pfach text DEFAULT NULL::text, zus text DEFAULT NULL::text, home boolean DEFAULT true, isperson boolean DEFAULT true) RETURNS TABLE(pfid smallint, is_home boolean, pstfach text, zs text, street text, plzahl character varying, ortid smallint, ortsname character varying, iso_3166_2 character varying, regionenname character varying, iso_land character, land_name character varying)
    LANGUAGE plpgsql
    AS $$
declare
  linkid  adressen.lnk_person_ort.lnk_per_ort_id%TYPE ;
  isoCode adressen.hub_land.iso_3166_1%type = case when (char_length(land)=2)
                                              then land
                                              else (select iso_3166_1 from adressen.hub_land where laendername=land)
                                              end ;
  ortId adressen.hub_ort.ort_id%type = adressen.getOrtId(stadt => stadt,countryCode => isoCode) ;
Begin
  SET CLIENT_ENCODING TO 'UTF8' ;
  SET search_path TO adressen,public;

  If (ortId is null)
  then begin
       --- ort must be defined in hub_ort first
       raise notice 'No ort_id fuer stadt = %', stadt;
       raise notice '  isoCode = %', isoCode;
  end ;
  else begin
       if isPerson
       then begin
	        select lnk_per_ort_id into linkid from lnk_person_ort where person_id = id and ort_id = ortID ;
            if (linkid is null)
            then begin
                 raise notice 'link_ort: inserting into lnk_person_ort values (%,%)', id,ortId ;
                 insert into adressen.lnk_person_ort (person_id,ort_id) values (id,ortId) returning lnk_per_ort_id into linkid  ;
                 end ;
            end if
            ;
            insert into adressen.sal_person_ort
            (lnk_per_ort_id,validity,postfach,zusatz,strasse,plz,ort_is_home)
            values
            (linkid,valty,pfach,zus,str,postzahl,home)
            ;
            end;
       else begin
	        --- checking if firma has address at ort. if not linke firma with ort
	        select lnk_fir_ort_id into linkid from lnk_firma_ort where firma_id = id and ort_id = ortID ;
	        if (linkid is null)
            then begin
	             raise notice 'link_ort: inserting into lnk_firma_ort values (%,%)', id,ortId ;
                 insert into adressen.lnk_firma_ort (firma_id,ort_id) values (id,ortId) returning lnk_fir_ort_id into linkid  ;
                 end ;
            end if
            ;
            insert into adressen.sal_firma_ort
            (lnk_fir_ort_id,validity,postfach,zusatz,strasse,plz)
            values
            (linkid,valty,pfach,zus,str,postzahl)
            ;
            end;
       end if
       ;
      end;
  end if
  ;
  RETURN QUERY select * from adressen.getfulladresse(isPerson,array[id]);
End ;
$$;


--
-- Name: link_photo(smallint, text); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.link_photo(pid smallint, filename text) RETURNS TABLE(pers_id smallint, rast oid)
    LANGUAGE plpgsql
    AS $$
Declare
  personHasPhoto constant bool  = (exists(select * from adressen.sat_person_bild where person_id=pid)) ;
Begin
  SET CLIENT_ENCODING TO 'UTF8' ;
  SET search_path TO adressen,public ;
  if (pid is null) then RAISE EXCEPTION 'linkPhoto: pid is null !' ;
  else Begin
       if (fileName = '') then raise notice 'linkPhoto: empty string as fileName: %',fileName ;
       else Begin
            if personHasPhoto
               then Begin
	                raise notice 'linkPhoto: Replacing existing photo of person % with %', pid, fileName ;
	                delete from adressen.sat_person_bild where  person_id = pid ;
	                end ;
               else raise notice 'linkPhoto: Adding to person % new photo %', pid, fileName ;
            end if ;
            insert into adressen.sat_person_bild(person_id,raster) values (pid,lo_import(fileName)) ;
            RETURN QUERY
            select person_id, raster
            from   adressen.sat_person_bild
            where  person_id = pid
            ;
            End ;
       end if ;
       End ;
  end if ;
End;
$$;


--
-- Name: link_tel(smallint, text[], boolean); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.link_tel(id smallint, telnummern text[], isperson boolean DEFAULT true) RETURNS TABLE(idnr smallint, telnr bigint, tel_is_home boolean, tel_typ character varying)
    LANGUAGE plpgsql
    AS $$
Declare
  telLinked bool ;
  telNr     adressen.hub_telefon.telefon%TYPE ;
Begin
  SET CLIENT_ENCODING TO 'UTF8' ;
  SET search_path TO adressen,public ;
  if (id is null) then RAISE EXCEPTION 'id is null !' ;
  else begin
	   if isperson
       then for i in 1 .. coalesce(array_upper(telnummern, 1),0)
       -- linking telnummern to person via lnk_person_telefon
            loop
	            telNr = adressen.insertNeueTelNr(telnummern[i]) ;
                telLinked = (exists(select * from adressen.lnk_person_telefon where person_id=id and telefon=telNr)) ;
                if telLinked
                then raise notice 'link_tel: telNr % already linked to person %',telNr, id ;
                else insert into adressen.lnk_person_telefon (person_id,telefon) values (id,telNr) ;
                end if ;
            end loop;
            RETURN QUERY
            select l.person_id
                  ,h.telefon
                  ,h.tel_is_home
                  ,h.tel_typ
            from   adressen.lnk_person_telefon l
                   inner join adressen.hub_telefon h on l.telefon = h.telefon
            where  person_id=id
            and  validity @> current_date
            order by telefon;
       else for i in 1 .. coalesce(array_upper(telnummern, 1),0)
       -- linking telnummern to firma via lnk_firma_telefon
            loop
	            telNr = adressen.insertNeueTelNr(telnummern[i]) ;
                telLinked = (exists(select * from adressen.lnk_firma_telefon where  firma_id=id and telefon=telNr)) ;
                if telLinked
                then raise notice 'link_tel: telNr % already linked to firma %',telNr, id ;
                else insert into adressen.lnk_firma_telefon (firma_id,telefon) values (id,telNr) ;
                end if ;
            end loop;
            RETURN QUERY
            select l.firma_id
                  ,h.telefon
                  ,h.tel_is_home
                  ,h.tel_typ
            from   adressen.lnk_firma_telefon l
                   inner join adressen.hub_telefon h on l.telefon = h.telefon
            where  l.firma_id=id
              and  validity @> current_date
            order by telefon ;
       end if ;
       end ;
  end if;
End;
$$;


--
-- Name: link_tel(integer, text[], boolean); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.link_tel(id integer, telnummern text[], isperson boolean DEFAULT false) RETURNS TABLE(idnr smallint, telnr bigint, tel_is_home boolean, tel_typ character varying)
    LANGUAGE plpgsql
    AS $$
Begin
  RETURN QUERY
  select *
  from   adressen.link_tel(cast(id  as smallint), cast(telnummern as text[]), isperson);
End;
$$;


--
-- Name: preparevcardfirma(integer[]); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.preparevcardfirma(fidarray integer[]) RETURNS SETOF adressen.vcard_type
    LANGUAGE sql
    AS $$
  SET CLIENT_ENCODING TO 'UTF8' ;
  SET search_path TO adressen,public ;
  with grp as
  (select lnk.firma_id
         ,STRING_AGG(grp.gruppen_name,',' order by grp.gruppen_name)    CATEGORIES
   from   adressen.hub_gruppe                    grp
          inner join adressen.lnk_firma_gruppe  lnk
          on grp.gruppen_id = lnk.gruppen_id
   where  lnk.firma_id = any(fidArray)
   group by lnk.firma_id
  )
  ,tel as
  (select lnk.firma_id
         ,STRING_AGG('TEL;TYPE=work,'||tel.tel_typ
                     ||':+'||tel.telefon
                    ,chr(13)||chr(10) order by tel.telefon
                    )                           fon
   from   adressen.lnk_firma_telefon             lnk
          inner join adressen.hub_telefon        tel
          on lnk.telefon = tel.telefon
   where  lnk.validity @> current_date
   group by lnk.firma_id
  )
  ,epo as
  (select firma_id
         ,STRING_AGG('EMAIL;TYPE=work:'|| epost
                    ,chr(13)||chr(10) order by epost
                    )                 Epost
   from   adressen.sat_firma_epost
   where  validity @> current_date
   group by firma_id
  )
  ,adr as
  (select id        firma_id
         ,STRING_AGG('ADR;TYPE=work'
                     ||':'||coalesce(Postfach,'')
                     ||';'||coalesce(Zusatz,'')
                     ||chr(13)||chr(10)||' '||';'||coalesce(Strasse,'')
                     ||chr(13)||chr(10)||' '||';'||coalesce(ortsname,'')
                     ||chr(13)||chr(10)||' '||';'||coalesce(regionenname,'')
                     ||chr(13)||chr(10)||' '||';'||coalesce(plz,'')
                     ||';'||coalesce(laendername,'')
                    ,chr(13)||chr(10) order by ort_is_home
                    )                                   adr
   from   adressen.getFullAdresse(person=>false,idArray=>fidArray)
   group by id
  )
  select fir.firma_id
        ,fir.firmenname                                 FN
        ,fir.firmenname                                 N
        ,''                                             NICKNAME
        ,''                                             BDAY
        ,fir.firmen_url
        ,adr.adr
        ,tel.fon
        ,epo.Epost
        ,grp.CATEGORIES
        ,replace(fir.Notiz,' ',chr(13)||chr(10)||'  ')
        ,''                                             PHOTO
  from   adressen.hub_firma fir
         left outer join adr
         on fir.firma_id = adr.firma_id
         left outer join tel
         on fir.firma_id = tel.firma_id
         left outer join epo
         on fir.firma_id = epo.firma_id
         left outer join grp
         on fir.firma_id = grp.firma_id
  where  fir.firma_id = any(fidArray)
  ;
$$;


--
-- Name: preparevcardperson(integer[], boolean); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.preparevcardperson(pidarray integer[], mitphotos boolean DEFAULT false) RETURNS SETOF adressen.vcard_type
    LANGUAGE sql
    AS $$
  SET CLIENT_ENCODING TO 'UTF8' ;
  SET search_path TO adressen,public ;
  with grp as
  (select lnk.person_id
         ,STRING_AGG(grp.gruppen_name,',' order by grp.gruppen_name)    CATEGORIES
   from   adressen.hub_gruppe                    grp
          inner join adressen.lnk_person_gruppe  lnk
          on grp.gruppen_id = lnk.gruppen_id
   where  lnk.person_id = any(pidArray)
   group by lnk.person_id
  )
  ,vorNachNam as
  (select person_id
         ,STRING_AGG(case when (namen_position>0) then nomen else null end,' ' order by namen_position)         Vornamen
         ,STRING_AGG(case when (namen_position<0) then nomen else null end,' ' order by namen_position desc)    Nachnamen
   from   adressen.sat_person_namen
   where  person_id = any(pidArray)
   group by person_id
  )
  ,nam as
  (select person_id
         ,Vornamen||' '||Nachnamen  FN
         ,Nachnamen||';'||Vornamen  N
   from   vorNachNam
  )
  ,tel as
  (select lnk.person_id
         ,STRING_AGG('TEL;TYPE='
                     ||case when (tel.tel_is_home) then 'home' else 'work' end
                     ||','||tel.tel_typ
                     ||':+'||tel.telefon
                    ,chr(13)||chr(10) order by tel.telefon
                    )                           fon
   from   adressen.lnk_person_telefon           lnk
          inner join adressen.hub_telefon       tel
          on lnk.telefon = tel.telefon
   where  lnk.validity @> current_date
   group by lnk.person_id
  )
  ,epo as
  (select person_id
         ,STRING_AGG('EMAIL;TYPE='||case when (epost_is_home) then 'home' else 'work' end||':'|| epost
                    ,chr(13)||chr(10) order by epost
                    )                 Epost
   from   adressen.sat_person_epost
   where  validity @> current_date
   group by person_id
  )
  ,adr as
  (select id        person_id
         ,STRING_AGG('ADR;TYPE='
                     ||case when (ort_is_home) then 'home' else 'work' end
                     ||':'||coalesce(Postfach,'')
                     ||';'||coalesce(Zusatz,'')
                     ||chr(13)||chr(10)||' '||';'||coalesce(Strasse,'')
                     ||chr(13)||chr(10)||' '||';'||coalesce(ortsname,'')
                     ||chr(13)||chr(10)||' '||';'||coalesce(regionenname,'')
                     ||chr(13)||chr(10)||' '||';'||coalesce(plz,'')
                     ||';'||coalesce(laendername,'')
                    ,chr(13)||chr(10) order by ort_is_home
                    )                                   adr
   from   adressen.getFullAdresse(person=>true,idArray=>pidArray)
   group by id
  )
  select per.person_id
        ,nam.FN
        ,nam.N
        ,per.spitzname                                  NICKNAME
        ,to_char(per.Geburtsdatum,'YYYYMMDD')
        ,''                                             URL
        ,adr.adr
        ,tel.fon
        ,epo.Epost
        ,grp.CATEGORIES
        ,per.Notiz
---- Contacts.app zeigt Photos nicht an :(
        ,case when (mitphotos)
--              then 'PHOTO;ENCODING=b;TYPE=JPEG:'
              then 'PHOTO;TYPE=JPEG;ENCODING=B:'
         ||chr(13)||chr(10)||' '
         ||regexp_replace(encode(lo_get(pho.raster),'base64'),chr(10),chr(13)||chr(10)||' ','g')
         else '' end PHOTO
  from   adressen.hub_person per
         inner join nam
         on per.person_id = nam.person_id
         left outer join adr
         on per.person_id = adr.person_id
         left outer join tel
         on per.person_id = tel.person_id
         left outer join epo
         on per.person_id = epo.person_id
         left outer join grp
         on per.person_id = grp.person_id
         left outer join adressen.sat_person_bild          pho
         on per.person_id = pho.person_id
         and mitphotos
  ;
$$;


--
-- Name: quotetext(text); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.quotetext(str text) RETURNS text
    LANGUAGE sql
    AS $$
  select adressen.surroundtext(str, '{"\"","\""}'::text[]) ;
$$;


--
-- Name: surroundtext(text, text[]); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.surroundtext(str text, surtext text[] DEFAULT '{(,)}'::text[]) RETURNS text
    LANGUAGE sql
    AS $$
  select surText[1]||adressen.trimtext(replace(str,chr(10),' '))||surText[2] ;
$$;


--
-- Name: trimtext(text); Type: FUNCTION; Schema: adressen; Owner: -
--

CREATE FUNCTION adressen.trimtext(str text) RETURNS text
    LANGUAGE sql
    AS $$
  select coalesce(trim(str),'');
$$;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: hub_person; Type: TABLE; Schema: adressen; Owner: -
--

CREATE TABLE adressen.hub_person (
    person_id smallint NOT NULL,
    spitzname character varying(16),
    geburtsdatum date,
    notiz text
);


--
-- Name: adresse; Type: VIEW; Schema: adressen; Owner: -
--

CREATE VIEW adressen.adresse AS
 SELECT per.person_id,
    nam.vornamen,
    nam.nachnamen,
    per.geburtsdatum,
    adr.postfach,
    adr.zusatz,
    adr.strasse,
    adr.plz,
    adr.ortsname AS ort,
    adr.regionenname AS region,
    adr.laendername AS land,
    adr.telefon,
    adr.epost,
    adr.categories,
    per.notiz
   FROM ((adressen.hub_person per
     JOIN adressen.getnamen() nam(person_id, vornamen, nachnamen) ON ((per.person_id = nam.person_id)))
     LEFT JOIN adressen.getfullinformation(person => true, idarray => NULL::integer[]) adr(id, ort_is_home, postfach, zusatz, strasse, plz, ort_id, ortsname, iso_3166_2, regionenname, iso_3166_1, laendername, telefon, epost, categories) ON ((per.person_id = adr.id)));


--
-- Name: hub_firma; Type: TABLE; Schema: adressen; Owner: -
--

CREATE TABLE adressen.hub_firma (
    firma_id smallint NOT NULL,
    firmenname character varying(64) NOT NULL,
    firmen_url character varying(128),
    notiz text
);


--
-- Name: hub_telefon; Type: TABLE; Schema: adressen; Owner: -
--

CREATE TABLE adressen.hub_telefon (
    telefon bigint NOT NULL,
    tel_is_home boolean DEFAULT true NOT NULL,
    tel_typ character varying(16) DEFAULT 'voice'::character varying NOT NULL
);


--
-- Name: lnk_firma_gruppe; Type: TABLE; Schema: adressen; Owner: -
--

CREATE TABLE adressen.lnk_firma_gruppe (
    lnk_firma_gruppe_id smallint NOT NULL,
    firma_id smallint,
    gruppen_id smallint
);


--
-- Name: lnk_firma_telefon; Type: TABLE; Schema: adressen; Owner: -
--

CREATE TABLE adressen.lnk_firma_telefon (
    lnk_firma_tel_id smallint NOT NULL,
    firma_id smallint,
    telefon bigint,
    validity daterange DEFAULT daterange(CURRENT_DATE, NULL::date) NOT NULL
);


--
-- Name: sat_firma_epost; Type: TABLE; Schema: adressen; Owner: -
--

CREATE TABLE adressen.sat_firma_epost (
    firma_id smallint NOT NULL,
    epost character varying(64) NOT NULL,
    validity daterange DEFAULT daterange(CURRENT_DATE, NULL::date) NOT NULL
);


--
-- Name: export_firmen; Type: VIEW; Schema: adressen; Owner: -
--

CREATE VIEW adressen.export_firmen AS
 WITH fid AS (
         SELECT lnk_firma_gruppe.firma_id
           FROM adressen.lnk_firma_gruppe
          WHERE (lnk_firma_gruppe.gruppen_id = 12)
        ), efirma AS (
         SELECT sat_firma_epost.firma_id,
            array_agg(ROW(sat_firma_epost.epost, NULL::character varying(8))::adressen.emailwithalias) AS epost
           FROM adressen.sat_firma_epost
          WHERE (sat_firma_epost.validity @> CURRENT_DATE)
          GROUP BY sat_firma_epost.firma_id
        ), telfirma AS (
         SELECT ltel.firma_id,
            array_agg(ROW((
                CASE
                    WHEN ((tel.tel_typ)::text <> 'voice'::text) THEN tel.tel_typ
                    WHEN tel.tel_is_home THEN 'home'::character varying
                    ELSE 'work'::character varying
                END)::text, tel.telefon)::adressen.typedphonenr) AS phone
           FROM (adressen.lnk_firma_telefon ltel
             JOIN adressen.hub_telefon tel ON (((ltel.telefon = tel.telefon) AND (ltel.validity @> CURRENT_DATE))))
          GROUP BY ltel.firma_id
        ), adrfirma AS (
         SELECT getfulladresse.id,
            array_agg(ROW(
                CASE
                    WHEN getfulladresse.ort_is_home THEN 'home'::text
                    ELSE 'work'::text
                END, getfulladresse.strasse, getfulladresse.zusatz, getfulladresse.postfach, getfulladresse.ortsname, getfulladresse.iso_3166_2, getfulladresse.plz, getfulladresse.laendername)::adressen.typedadress) AS address
           FROM adressen.getfulladresse(person => false) getfulladresse(id, ort_is_home, postfach, zusatz, strasse, plz, ort_id, ortsname, iso_3166_2, regionenname, iso_3166_1, laendername)
          GROUP BY getfulladresse.id
        )
 SELECT fid.firma_id AS id,
    h.firmenname,
    h.firmen_url,
    telfirma.phone,
    adrfirma.address,
    efirma.epost,
    h.notiz
   FROM ((((fid
     LEFT JOIN efirma ON ((fid.firma_id = efirma.firma_id)))
     LEFT JOIN adressen.hub_firma h ON ((fid.firma_id = h.firma_id)))
     LEFT JOIN telfirma ON ((fid.firma_id = telfirma.firma_id)))
     LEFT JOIN adrfirma ON ((fid.firma_id = adrfirma.id)));


--
-- Name: lnk_person_gruppe; Type: TABLE; Schema: adressen; Owner: -
--

CREATE TABLE adressen.lnk_person_gruppe (
    lnk_person_gruppe_id smallint NOT NULL,
    person_id smallint,
    gruppen_id smallint
);


--
-- Name: lnk_person_telefon; Type: TABLE; Schema: adressen; Owner: -
--

CREATE TABLE adressen.lnk_person_telefon (
    lnk_person_tel_id smallint NOT NULL,
    person_id smallint,
    telefon bigint,
    validity daterange DEFAULT daterange(CURRENT_DATE, NULL::date) NOT NULL
);


--
-- Name: sat_person_epost; Type: TABLE; Schema: adressen; Owner: -
--

CREATE TABLE adressen.sat_person_epost (
    person_id smallint NOT NULL,
    epost character varying(64) NOT NULL,
    epost_is_home boolean DEFAULT true NOT NULL,
    validity daterange DEFAULT daterange(CURRENT_DATE, NULL::date) NOT NULL,
    mail_alias character varying(8)
);


--
-- Name: COLUMN sat_person_epost.mail_alias; Type: COMMENT; Schema: adressen; Owner: -
--

COMMENT ON COLUMN adressen.sat_person_epost.mail_alias IS 'Mail alias fuer bbdb';


--
-- Name: export_personen; Type: VIEW; Schema: adressen; Owner: -
--

CREATE VIEW adressen.export_personen AS
 WITH pid AS (
         SELECT lnk_person_gruppe.person_id
           FROM adressen.lnk_person_gruppe
          WHERE (lnk_person_gruppe.gruppen_id = 12)
        ), e AS (
         SELECT sat_person_epost.person_id,
            array_agg(ROW(sat_person_epost.epost, sat_person_epost.mail_alias)::adressen.emailwithalias) AS epost
           FROM adressen.sat_person_epost
          WHERE (sat_person_epost.validity @> CURRENT_DATE)
          GROUP BY sat_person_epost.person_id
        ), tel AS (
         SELECT ltel.person_id,
            array_agg(ROW((
                CASE
                    WHEN ((tel_1.tel_typ)::text <> 'voice'::text) THEN tel_1.tel_typ
                    WHEN tel_1.tel_is_home THEN 'home'::character varying
                    ELSE 'work'::character varying
                END)::text, tel_1.telefon)::adressen.typedphonenr) AS phone
           FROM (adressen.lnk_person_telefon ltel
             JOIN adressen.hub_telefon tel_1 ON (((ltel.telefon = tel_1.telefon) AND (ltel.validity @> CURRENT_DATE))))
          GROUP BY ltel.person_id
        ), adr AS (
         SELECT getfulladresse.id,
            array_agg(ROW(
                CASE
                    WHEN getfulladresse.ort_is_home THEN 'home'::text
                    ELSE 'work'::text
                END, getfulladresse.strasse, getfulladresse.zusatz, getfulladresse.postfach, getfulladresse.ortsname, getfulladresse.iso_3166_2, getfulladresse.plz, getfulladresse.laendername)::adressen.typedadress) AS address
           FROM adressen.getfulladresse() getfulladresse(id, ort_is_home, postfach, zusatz, strasse, plz, ort_id, ortsname, iso_3166_2, regionenname, iso_3166_1, laendername)
          GROUP BY getfulladresse.id
        )
 SELECT pid.person_id AS id,
    p.vornamen AS vorname,
    p.nachnamen AS nachname,
    h.spitzname,
    h.geburtsdatum,
    tel.phone,
    adr.address,
    e.epost,
    h.notiz
   FROM (((((pid
     LEFT JOIN adressen.getnamen(maxvornamen => 1, maxnachnamen => 1) p(person_id, vornamen, nachnamen) ON ((pid.person_id = p.person_id)))
     LEFT JOIN e ON ((pid.person_id = e.person_id)))
     LEFT JOIN adressen.hub_person h ON ((pid.person_id = h.person_id)))
     LEFT JOIN tel ON ((pid.person_id = tel.person_id)))
     LEFT JOIN adr ON ((pid.person_id = adr.id)));


--
-- Name: bbdb; Type: VIEW; Schema: adressen; Owner: -
--

CREATE VIEW adressen.bbdb AS
 WITH telexplode AS (
         SELECT ((0)::smallint - export_firmen.id) AS id,
            unnest(export_firmen.phone) AS typedphone
           FROM adressen.export_firmen
        UNION ALL
         SELECT export_personen.id,
            unnest(export_personen.phone) AS typedphone
           FROM adressen.export_personen
        ), tel AS (
         SELECT telexplode.id,
            string_agg(adressen.surroundtext((adressen.getbbdbfield((telexplode.typedphone).typ) || adressen.getbbdbfield(((telexplode.typedphone).telnr)::text)), '{[,]}'::text[]), ' '::text) AS phone
           FROM telexplode
          GROUP BY telexplode.id
        ), adrexplode AS (
         SELECT ((0)::smallint - export_firmen.id) AS id,
            unnest(export_firmen.address) AS typedaddress
           FROM adressen.export_firmen
        UNION ALL
         SELECT export_personen.id,
            unnest(export_personen.address) AS typedaddress
           FROM adressen.export_personen
        ), adr AS (
         SELECT adrexplode.id,
            string_agg(adressen.surroundtext(((((((adressen.getbbdbfield((adrexplode.typedaddress).typ) || adressen.surroundtext(((adressen.getbbdbfield((adrexplode.typedaddress).strasse, true) || adressen.getbbdbfield((adrexplode.typedaddress).zusatz)) || adressen.getbbdbfield((adrexplode.typedaddress).postfach)))) || ' '::text) || adressen.getbbdbfield(((adrexplode.typedaddress).ortsname)::text)) || adressen.getbbdbfield(((adrexplode.typedaddress).iso_3166_2)::text)) || adressen.getbbdbfield(((adrexplode.typedaddress).plz)::text)) || adressen.getbbdbfield(((adrexplode.typedaddress).laendername)::text)), '{[,]}'::text[]), ' '::text) AS adresse
           FROM adrexplode
          GROUP BY adrexplode.id
        ), epostexplode AS (
         SELECT ((0)::smallint - export_firmen.id) AS id,
            unnest(export_firmen.epost) AS email
           FROM adressen.export_firmen
        UNION ALL
         SELECT export_personen.id,
            unnest(export_personen.epost) AS email
           FROM adressen.export_personen
        ), eml AS (
         SELECT epostexplode.id,
            string_agg(adressen.getbbdbfield(((epostexplode.email).epost)::text), ' '::text) AS epost,
            adressen.trimtext(string_agg(adressen.trimtext(((epostexplode.email).alias)::text), ' '::text)) AS alias
           FROM epostexplode
          GROUP BY epostexplode.id
        ), evw AS (
         SELECT export_personen.id,
            export_personen.spitzname,
            export_personen.vorname,
            export_personen.nachname,
            export_personen.notiz
           FROM adressen.export_personen
        UNION ALL
         SELECT ((0)::smallint - export_firmen.id),
            NULL::character varying(16) AS spitzname,
            export_firmen.firmenname AS vorname,
            export_firmen.firmen_url AS nachname,
            export_firmen.notiz
           FROM adressen.export_firmen
        )
 SELECT (- (9999)::smallint) AS id,
    NULL::character varying(16) AS spitzname,
    ';; -*- mode: emacs-lisp; coding: utf-8; -*-'::text AS bbdb_record
UNION ALL
 SELECT (- (8888)::smallint) AS id,
    NULL::character varying(16) AS spitzname,
    ';;; file-format: 9'::text AS bbdb_record
UNION ALL
 SELECT evw.id,
    evw.spitzname,
    adressen.surroundtext((((((((((((((adressen.getbbdbfield(evw.vorname, true) || adressen.getbbdbfield(evw.nachname, true)) || 'nil nil nil '::text) || adressen.surroundtext(tel.phone)) || ' '::text) || adressen.surroundtext(adr.adresse)) || ' '::text) || adressen.surroundtext(eml.epost)) || adressen.surroundtext(((adressen.surroundtext(('notes . '::text || adressen.quotetext(evw.notiz))) || ' '::text) ||
        CASE
            WHEN (char_length(eml.alias) > 0) THEN adressen.surroundtext(('mail-alias . '::text || adressen.quotetext(eml.alias)))
            ELSE NULL::text
        END))) || ' '::text) || adressen.getbbdbfield(('id-'::text || evw.id))) || adressen.getbbdbfield(to_char(CURRENT_TIMESTAMP, 'yyyy-mm-dd hh24:mi:ss of'::text))) || adressen.getbbdbfield(to_char(CURRENT_TIMESTAMP, 'yyyy-mm-dd hh24:mi:ss of'::text))) || 'nil'::text), '{[,]}'::text[]) AS bbdb_record
   FROM (((evw
     LEFT JOIN tel ON ((evw.id = tel.id)))
     LEFT JOIN adr ON ((evw.id = adr.id)))
     LEFT JOIN eml ON ((evw.id = eml.id)));


--
-- Name: firma; Type: VIEW; Schema: adressen; Owner: -
--

CREATE VIEW adressen.firma AS
 SELECT fir.firma_id,
    fir.firmenname,
    fir.firmen_url,
    adr.postfach,
    adr.zusatz,
    adr.strasse,
    adr.plz,
    adr.ortsname AS ort,
    adr.regionenname AS region,
    adr.laendername AS land,
    adr.telefon,
    adr.epost,
    adr.categories,
    fir.notiz
   FROM (adressen.hub_firma fir
     LEFT JOIN adressen.getfullinformation(person => false, idarray => NULL::integer[]) adr(id, ort_is_home, postfach, zusatz, strasse, plz, ort_id, ortsname, iso_3166_2, regionenname, iso_3166_1, laendername, telefon, epost, categories) ON ((fir.firma_id = adr.id)));


--
-- Name: hub_gruppe; Type: TABLE; Schema: adressen; Owner: -
--

CREATE TABLE adressen.hub_gruppe (
    gruppen_id smallint NOT NULL,
    gruppen_name character varying(16),
    kommentar text
);


--
-- Name: sat_person_namen; Type: TABLE; Schema: adressen; Owner: -
--

CREATE TABLE adressen.sat_person_namen (
    person_id smallint NOT NULL,
    namen_position smallint NOT NULL,
    nomen character varying(32) NOT NULL
);


--
-- Name: geburtstag; Type: VIEW; Schema: adressen; Owner: -
--

CREATE VIEW adressen.geburtstag AS
 WITH pid AS (
         SELECT DISTINCT lnk_person_gruppe.person_id
           FROM adressen.lnk_person_gruppe
          WHERE (lnk_person_gruppe.gruppen_id IN ( SELECT hub_gruppe.gruppen_id
                   FROM adressen.hub_gruppe
                  WHERE ((hub_gruppe.gruppen_name)::text = 'Geburtstag'::text)))
        ), nam AS (
         SELECT sat_person_namen.person_id,
            string_agg((
                CASE
                    WHEN (sat_person_namen.namen_position > 0) THEN sat_person_namen.nomen
                    ELSE NULL::character varying
                END)::text, ' '::text ORDER BY sat_person_namen.namen_position) AS vorname,
            string_agg((
                CASE
                    WHEN (sat_person_namen.namen_position < 0) THEN sat_person_namen.nomen
                    ELSE NULL::character varying
                END)::text, ' '::text ORDER BY sat_person_namen.namen_position DESC) AS zuname
           FROM adressen.sat_person_namen
          WHERE (sat_person_namen.person_id IN ( SELECT pid.person_id
                   FROM pid))
          GROUP BY sat_person_namen.person_id
        )
 SELECT (date_part('month'::text, p.geburtsdatum))::smallint AS monat,
    (date_part('day'::text, p.geburtsdatum))::smallint AS tag,
    p.person_id,
    p.spitzname,
    nam.vorname,
    nam.zuname,
    (date_part('year'::text, CURRENT_DATE) - date_part('year'::text, p.geburtsdatum)) AS age
   FROM (adressen.hub_person p
     JOIN nam ON ((p.person_id = nam.person_id)))
  WHERE (p.geburtsdatum IS NOT NULL);


--
-- Name: html_firma; Type: VIEW; Schema: adressen; Owner: -
--

CREATE VIEW adressen.html_firma AS
 WITH tel AS (
         SELECT tel_1.firma_id,
            string_agg(((((((
                CASE typ.tel_typ
                    WHEN 'voice'::text THEN 'tel'::character varying
                    WHEN 'cell'::text THEN 'mobil'::character varying
                    ELSE typ.tel_typ
                END)::text || ':<a href="tel:+'::text) || tel_1.telefon) || '">+'::text) || tel_1.telefon) || '</a>'::text), ' '::text ORDER BY tel_1.telefon) AS telefon
           FROM (adressen.lnk_firma_telefon tel_1
             JOIN adressen.hub_telefon typ ON ((typ.telefon = tel_1.telefon)))
          WHERE (tel_1.validity @> CURRENT_DATE)
          GROUP BY tel_1.firma_id
        ), eml AS (
         SELECT sat_firma_epost.firma_id,
            string_agg((((('<a href="mailto:'::text || (sat_firma_epost.epost)::text) || '?Subject=Anfrage">'::text) || (sat_firma_epost.epost)::text) || '</a>'::text), ' '::text ORDER BY sat_firma_epost.epost) AS epost
           FROM adressen.sat_firma_epost
          WHERE (sat_firma_epost.validity @> CURRENT_DATE)
          GROUP BY sat_firma_epost.firma_id
        ), fir AS (
         SELECT firma.categories,
            firma.land,
            firma.region,
            firma.firma_id,
            firma.firmenname,
            COALESCE((((((('<a href="'::text || (firma.firmen_url)::text) || '">'::text) || (firma.firmenname)::text) || ' , '::text) || (firma.region)::text) || '</a>'::text), (((firma.firmenname)::text || ' , '::text) || (firma.region)::text)) AS dt,
            (((COALESCE((firma.strasse || ' , '::text), ''::text) || (firma.plz)::text) || ' '::text) || (firma.ort)::text) AS adr,
            firma.notiz
           FROM adressen.firma
        )
 SELECT fir.categories,
    fir.land,
    fir.region,
    fir.firma_id,
    fir.firmenname,
    (((((((((('<dt>'::text || fir.dt) || '</dt>'::text) || chr(10)) || '<dd>'::text) || fir.adr) || chr(10)) || COALESCE((' , '::text || tel.telefon), ''::text)) || COALESCE((' , '::text || eml.epost), ''::text)) || COALESCE(((('<br />'::text || chr(10)) || '<i>'::text) || fir.notiz), (''::text || '<i>'::text))) || '</dd>'::text) AS html
   FROM ((fir
     LEFT JOIN tel ON ((fir.firma_id = tel.firma_id)))
     LEFT JOIN eml ON ((fir.firma_id = eml.firma_id)));


--
-- Name: hub_firma_firma_id_seq; Type: SEQUENCE; Schema: adressen; Owner: -
--

CREATE SEQUENCE adressen.hub_firma_firma_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: hub_firma_firma_id_seq; Type: SEQUENCE OWNED BY; Schema: adressen; Owner: -
--

ALTER SEQUENCE adressen.hub_firma_firma_id_seq OWNED BY adressen.hub_firma.firma_id;


--
-- Name: hub_gruppe_gruppen_id_seq; Type: SEQUENCE; Schema: adressen; Owner: -
--

CREATE SEQUENCE adressen.hub_gruppe_gruppen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: hub_gruppe_gruppen_id_seq; Type: SEQUENCE OWNED BY; Schema: adressen; Owner: -
--

ALTER SEQUENCE adressen.hub_gruppe_gruppen_id_seq OWNED BY adressen.hub_gruppe.gruppen_id;


--
-- Name: hub_land; Type: TABLE; Schema: adressen; Owner: -
--

CREATE TABLE adressen.hub_land (
    iso_3166_1 character(2) NOT NULL,
    laendername character varying(64) NOT NULL,
    laendername2 character varying(64)
);


--
-- Name: hub_ort; Type: TABLE; Schema: adressen; Owner: -
--

CREATE TABLE adressen.hub_ort (
    ort_id smallint NOT NULL,
    ortsname character varying(64) NOT NULL,
    parentort smallint,
    iso_3166_1 character(2),
    iso_3166_2 character varying(4),
    CONSTRAINT parent_or_region CHECK ((((parentort IS NOT NULL) AND (iso_3166_2 IS NULL)) OR ((parentort IS NULL) AND (iso_3166_2 IS NOT NULL))))
);


--
-- Name: hub_ort_ort_id_seq; Type: SEQUENCE; Schema: adressen; Owner: -
--

CREATE SEQUENCE adressen.hub_ort_ort_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: hub_ort_ort_id_seq; Type: SEQUENCE OWNED BY; Schema: adressen; Owner: -
--

ALTER SEQUENCE adressen.hub_ort_ort_id_seq OWNED BY adressen.hub_ort.ort_id;


--
-- Name: hub_person_person_id_seq; Type: SEQUENCE; Schema: adressen; Owner: -
--

CREATE SEQUENCE adressen.hub_person_person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: hub_person_person_id_seq; Type: SEQUENCE OWNED BY; Schema: adressen; Owner: -
--

ALTER SEQUENCE adressen.hub_person_person_id_seq OWNED BY adressen.hub_person.person_id;


--
-- Name: hub_region; Type: TABLE; Schema: adressen; Owner: -
--

CREATE TABLE adressen.hub_region (
    iso_3166_1 character(2) NOT NULL,
    iso_3166_2 character varying(4) NOT NULL,
    regionenname character varying(64) NOT NULL,
    regionenname2 character varying(64)
);


--
-- Name: hub_telefon_typ; Type: TABLE; Schema: adressen; Owner: -
--

CREATE TABLE adressen.hub_telefon_typ (
    tel_typ character varying(16) NOT NULL,
    tel_typ_kom text
);


--
-- Name: latex_geburtstag; Type: VIEW; Schema: adressen; Owner: -
--

CREATE VIEW adressen.latex_geburtstag AS
 WITH mon AS (
         SELECT geburtstag.monat,
            count(*) AS anz
           FROM adressen.geburtstag
          GROUP BY geburtstag.monat
        ), geb AS (
         SELECT geburtstag.vorname,
            geburtstag.zuname,
            (geburtstag.vorname || geburtstag.zuname) AS vorzuname,
            COALESCE(geburtstag.spitzname, (((geburtstag.vorname || ' '::text) || geburtstag.zuname))::character varying) AS displayname,
            geburtstag.monat,
            geburtstag.tag,
            (geburtstag.age)::smallint AS age
           FROM adressen.geburtstag
        )
 SELECT mon.monat,
    geb.tag,
    geb.vorname,
    (((((((
        CASE geb.vorzuname
            WHEN first_value(geb.vorzuname) OVER (PARTITION BY mon.monat ORDER BY mon.monat, geb.tag, geb.vorname) THEN (((((((chr(10) || '\begin{tabular}{|l|rlr}\cline{1-1}'::text) || chr(10)) || '\multirow{'::text) || mon.anz) || '}{*}{\begin{sideways}\DTMgermanshortmonthname{'::text) || mon.monat) || '}\end{sideways}}'::text)
            ELSE ' '::text
        END || ' & '::text) || (
        CASE
            WHEN (0 = ((geb.age)::integer % 10)) THEN ((('\rund{'::text || geb.tag) || '}'::text))::bpchar
            ELSE (geb.tag)::character(2)
        END)::text) || ' & '::text) || (
        CASE
            WHEN (0 = ((geb.age)::integer % 10)) THEN ((('\rund{'::text || (geb.displayname)::text) || '}'::text))::character varying
            ELSE geb.displayname
        END)::text) || ' &'::text) || (
        CASE
            WHEN (0 = ((geb.age)::integer % 10)) THEN ((('\rund{'::text || geb.age) || '}'::text))::bpchar
            ELSE (geb.age)::character(2)
        END)::text) ||
        CASE geb.vorzuname
            WHEN last_value(geb.vorzuname) OVER (PARTITION BY mon.monat ORDER BY mon.monat, geb.tag, geb.vorname ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING) THEN ((((('\\\cline{1-1}'::text || chr(10)) || '\end{tabular}'::text) || chr(10)) || chr(10)) ||
            CASE mon.monat
                WHEN 6 THEN '\columnbreak'::text
                ELSE '\vspace{2ex}'::text
            END)
            ELSE '\\'::text
        END) AS latex
   FROM (mon
     JOIN geb ON ((mon.monat = geb.monat)));


--
-- Name: lnk_firma_gruppe_lnk_firma_gruppe_id_seq; Type: SEQUENCE; Schema: adressen; Owner: -
--

CREATE SEQUENCE adressen.lnk_firma_gruppe_lnk_firma_gruppe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lnk_firma_gruppe_lnk_firma_gruppe_id_seq; Type: SEQUENCE OWNED BY; Schema: adressen; Owner: -
--

ALTER SEQUENCE adressen.lnk_firma_gruppe_lnk_firma_gruppe_id_seq OWNED BY adressen.lnk_firma_gruppe.lnk_firma_gruppe_id;


--
-- Name: lnk_firma_ort; Type: TABLE; Schema: adressen; Owner: -
--

CREATE TABLE adressen.lnk_firma_ort (
    lnk_fir_ort_id smallint NOT NULL,
    firma_id smallint NOT NULL,
    ort_id smallint NOT NULL
);


--
-- Name: lnk_firma_ort_lnk_fir_ort_id_seq; Type: SEQUENCE; Schema: adressen; Owner: -
--

CREATE SEQUENCE adressen.lnk_firma_ort_lnk_fir_ort_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lnk_firma_ort_lnk_fir_ort_id_seq; Type: SEQUENCE OWNED BY; Schema: adressen; Owner: -
--

ALTER SEQUENCE adressen.lnk_firma_ort_lnk_fir_ort_id_seq OWNED BY adressen.lnk_firma_ort.lnk_fir_ort_id;


--
-- Name: lnk_firma_telefon_lnk_firma_typ_seq; Type: SEQUENCE; Schema: adressen; Owner: -
--

CREATE SEQUENCE adressen.lnk_firma_telefon_lnk_firma_typ_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lnk_firma_telefon_lnk_firma_typ_seq; Type: SEQUENCE OWNED BY; Schema: adressen; Owner: -
--

ALTER SEQUENCE adressen.lnk_firma_telefon_lnk_firma_typ_seq OWNED BY adressen.lnk_firma_telefon.lnk_firma_tel_id;


--
-- Name: lnk_person_gruppe_lnk_person_gruppe_id_seq; Type: SEQUENCE; Schema: adressen; Owner: -
--

CREATE SEQUENCE adressen.lnk_person_gruppe_lnk_person_gruppe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lnk_person_gruppe_lnk_person_gruppe_id_seq; Type: SEQUENCE OWNED BY; Schema: adressen; Owner: -
--

ALTER SEQUENCE adressen.lnk_person_gruppe_lnk_person_gruppe_id_seq OWNED BY adressen.lnk_person_gruppe.lnk_person_gruppe_id;


--
-- Name: lnk_person_ort; Type: TABLE; Schema: adressen; Owner: -
--

CREATE TABLE adressen.lnk_person_ort (
    lnk_per_ort_id smallint NOT NULL,
    person_id smallint NOT NULL,
    ort_id smallint NOT NULL
);


--
-- Name: lnk_person_ort_lnk_per_ort_id_seq; Type: SEQUENCE; Schema: adressen; Owner: -
--

CREATE SEQUENCE adressen.lnk_person_ort_lnk_per_ort_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lnk_person_ort_lnk_per_ort_id_seq; Type: SEQUENCE OWNED BY; Schema: adressen; Owner: -
--

ALTER SEQUENCE adressen.lnk_person_ort_lnk_per_ort_id_seq OWNED BY adressen.lnk_person_ort.lnk_per_ort_id;


--
-- Name: lnk_person_telefon_lnk_person_typ_seq; Type: SEQUENCE; Schema: adressen; Owner: -
--

CREATE SEQUENCE adressen.lnk_person_telefon_lnk_person_typ_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lnk_person_telefon_lnk_person_typ_seq; Type: SEQUENCE OWNED BY; Schema: adressen; Owner: -
--

ALTER SEQUENCE adressen.lnk_person_telefon_lnk_person_typ_seq OWNED BY adressen.lnk_person_telefon.lnk_person_tel_id;


--
-- Name: orte; Type: VIEW; Schema: adressen; Owner: -
--

CREATE VIEW adressen.orte AS
 WITH RECURSIVE cte AS (
         SELECT hub_ort.ort_id,
            (hub_ort.ortsname)::text AS ortsname,
            0 AS niveau,
            hub_ort.iso_3166_1,
            hub_ort.iso_3166_2,
            true AS root
           FROM adressen.hub_ort
          WHERE (hub_ort.parentort IS NULL)
        UNION ALL
         SELECT ch.ort_id,
            ((cte_1.ortsname || ' | '::text) || (ch.ortsname)::text) AS ortsname,
            (1 + cte_1.niveau) AS niveau,
            cte_1.iso_3166_1,
            cte_1.iso_3166_2,
            false AS root
           FROM (cte cte_1
             JOIN adressen.hub_ort ch ON ((ch.parentort = cte_1.ort_id)))
        )
 SELECT ort_id,
    ortsname,
    iso_3166_1,
    iso_3166_2,
    niveau,
    root,
    (NOT (ort_id IN ( SELECT hub_ort.parentort
           FROM adressen.hub_ort
          WHERE (hub_ort.parentort IS NOT NULL)))) AS leaf
   FROM cte;


--
-- Name: sal_firma_ort; Type: TABLE; Schema: adressen; Owner: -
--

CREATE TABLE adressen.sal_firma_ort (
    lnk_fir_ort_id smallint NOT NULL,
    postfach text,
    zusatz text,
    strasse text,
    plz character varying(16) NOT NULL,
    validity daterange DEFAULT daterange(CURRENT_DATE, NULL::date) NOT NULL
);


--
-- Name: sal_person_ort; Type: TABLE; Schema: adressen; Owner: -
--

CREATE TABLE adressen.sal_person_ort (
    lnk_per_ort_id smallint NOT NULL,
    postfach text,
    zusatz text,
    strasse text NOT NULL,
    plz character varying(16) NOT NULL,
    ort_is_home boolean DEFAULT true NOT NULL,
    validity daterange DEFAULT daterange(CURRENT_DATE, NULL::date) NOT NULL
);


--
-- Name: sat_person_bild; Type: TABLE; Schema: adressen; Owner: -
--

CREATE TABLE adressen.sat_person_bild (
    person_id smallint NOT NULL,
    raster oid
);


--
-- Name: smallint_serial; Type: SEQUENCE; Schema: adressen; Owner: -
--

CREATE SEQUENCE adressen.smallint_serial
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    CYCLE;


--
-- Name: vcards; Type: VIEW; Schema: adressen; Owner: -
--

CREATE VIEW adressen.vcards AS
 WITH pids AS (
         SELECT DISTINCT lnk_person_gruppe.person_id
           FROM adressen.lnk_person_gruppe
          WHERE (lnk_person_gruppe.gruppen_id IN ( SELECT hub_gruppe.gruppen_id
                   FROM adressen.hub_gruppe
                  WHERE ((hub_gruppe.gruppen_name)::text ~~ 'Kontakte%'::text)))
        ), n AS (
         SELECT getnamen.person_id,
            getnamen.vornamen,
            getnamen.nachnamen,
            (((replace(regexp_replace(getnamen.vornamen, '(''|\(|\))'::text, ''::text, 'g'::text), ' '::text, '_'::text) || '-'::text) || replace(regexp_replace(getnamen.nachnamen, '(''|\(|\))'::text, ''::text, 'g'::text), ' '::text, '_'::text)) || '.vcf'::text) AS filename
           FROM adressen.getnamen((( SELECT array_agg(pids.person_id) AS array_agg
                   FROM pids))::integer[]) getnamen(person_id, vornamen, nachnamen)
        )
 SELECT p.person_id,
    n.vornamen,
    n.nachnamen,
    p.geburtsdatum,
    (((((((('psql -U '::text || (USER)::text) || ' -d '::text) || (current_database())::text) || ' -tA -o vcards/'::text) || n.filename) || ' -c "select vcard from adressen.vcards where person_id='::text) || p.person_id) || ' ;"'::text) AS export_command,
    adressen.exportvcard((p.person_id)::integer, true, false) AS vcard
   FROM (adressen.hub_person p
     JOIN n ON ((p.person_id = n.person_id)));


--
-- Name: wanderlust; Type: VIEW; Schema: adressen; Owner: -
--

CREATE VIEW adressen.wanderlust AS
 WITH t AS (
         SELECT unnest(export_personen.epost) AS email,
            rtrim(adressen.getbbdbfield((COALESCE(export_personen.spitzname, (export_personen.vorname)::character varying))::text)) AS nickname,
            rtrim(adressen.getbbdbfield(((export_personen.vorname || ' '::text) || export_personen.nachname))) AS realname
           FROM adressen.export_personen
        UNION ALL
         SELECT unnest(export_firmen.epost) AS email,
            rtrim(adressen.getbbdbfield((export_firmen.firmenname)::text)) AS nickname,
            rtrim(adressen.getbbdbfield((export_firmen.firmenname)::text)) AS realname
           FROM adressen.export_firmen
        )
 SELECT (email).epost AS email,
    nickname,
    realname
   FROM t;


--
-- Name: hub_firma firma_id; Type: DEFAULT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.hub_firma ALTER COLUMN firma_id SET DEFAULT nextval('adressen.hub_firma_firma_id_seq'::regclass);


--
-- Name: hub_gruppe gruppen_id; Type: DEFAULT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.hub_gruppe ALTER COLUMN gruppen_id SET DEFAULT nextval('adressen.hub_gruppe_gruppen_id_seq'::regclass);


--
-- Name: hub_ort ort_id; Type: DEFAULT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.hub_ort ALTER COLUMN ort_id SET DEFAULT nextval('adressen.hub_ort_ort_id_seq'::regclass);


--
-- Name: hub_person person_id; Type: DEFAULT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.hub_person ALTER COLUMN person_id SET DEFAULT nextval('adressen.hub_person_person_id_seq'::regclass);


--
-- Name: lnk_firma_gruppe lnk_firma_gruppe_id; Type: DEFAULT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_firma_gruppe ALTER COLUMN lnk_firma_gruppe_id SET DEFAULT nextval('adressen.lnk_firma_gruppe_lnk_firma_gruppe_id_seq'::regclass);


--
-- Name: lnk_firma_ort lnk_fir_ort_id; Type: DEFAULT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_firma_ort ALTER COLUMN lnk_fir_ort_id SET DEFAULT nextval('adressen.lnk_firma_ort_lnk_fir_ort_id_seq'::regclass);


--
-- Name: lnk_firma_telefon lnk_firma_tel_id; Type: DEFAULT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_firma_telefon ALTER COLUMN lnk_firma_tel_id SET DEFAULT nextval('adressen.lnk_firma_telefon_lnk_firma_typ_seq'::regclass);


--
-- Name: lnk_person_gruppe lnk_person_gruppe_id; Type: DEFAULT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_person_gruppe ALTER COLUMN lnk_person_gruppe_id SET DEFAULT nextval('adressen.lnk_person_gruppe_lnk_person_gruppe_id_seq'::regclass);


--
-- Name: lnk_person_ort lnk_per_ort_id; Type: DEFAULT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_person_ort ALTER COLUMN lnk_per_ort_id SET DEFAULT nextval('adressen.lnk_person_ort_lnk_per_ort_id_seq'::regclass);


--
-- Name: lnk_person_telefon lnk_person_tel_id; Type: DEFAULT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_person_telefon ALTER COLUMN lnk_person_tel_id SET DEFAULT nextval('adressen.lnk_person_telefon_lnk_person_typ_seq'::regclass);


--
-- Name: hub_firma hub_firma_pkey; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.hub_firma
    ADD CONSTRAINT hub_firma_pkey PRIMARY KEY (firma_id);


--
-- Name: hub_gruppe hub_gruppe_pkey; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.hub_gruppe
    ADD CONSTRAINT hub_gruppe_pkey PRIMARY KEY (gruppen_id);


--
-- Name: hub_land hub_land_pkey; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.hub_land
    ADD CONSTRAINT hub_land_pkey PRIMARY KEY (iso_3166_1);


--
-- Name: hub_ort hub_ort_pkey; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.hub_ort
    ADD CONSTRAINT hub_ort_pkey PRIMARY KEY (ort_id);


--
-- Name: hub_ort hub_ort_uni; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.hub_ort
    ADD CONSTRAINT hub_ort_uni UNIQUE (ortsname, parentort, iso_3166_1, iso_3166_2);


--
-- Name: hub_person hub_person_pkey; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.hub_person
    ADD CONSTRAINT hub_person_pkey PRIMARY KEY (person_id);


--
-- Name: hub_region hub_region_pk; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.hub_region
    ADD CONSTRAINT hub_region_pk PRIMARY KEY (iso_3166_1, iso_3166_2);


--
-- Name: hub_telefon hub_telefon_pkey; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.hub_telefon
    ADD CONSTRAINT hub_telefon_pkey PRIMARY KEY (telefon);


--
-- Name: hub_telefon_typ hub_telefon_typ_pkey; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.hub_telefon_typ
    ADD CONSTRAINT hub_telefon_typ_pkey PRIMARY KEY (tel_typ);


--
-- Name: lnk_firma_gruppe lnk_firma_gruppe_pkey; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_firma_gruppe
    ADD CONSTRAINT lnk_firma_gruppe_pkey PRIMARY KEY (lnk_firma_gruppe_id);


--
-- Name: lnk_firma_gruppe lnk_firma_gruppe_uni; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_firma_gruppe
    ADD CONSTRAINT lnk_firma_gruppe_uni UNIQUE (firma_id, gruppen_id);


--
-- Name: lnk_firma_ort lnk_firma_ort_pkey; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_firma_ort
    ADD CONSTRAINT lnk_firma_ort_pkey PRIMARY KEY (lnk_fir_ort_id);


--
-- Name: lnk_firma_telefon lnk_firma_telefon_pkey; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_firma_telefon
    ADD CONSTRAINT lnk_firma_telefon_pkey PRIMARY KEY (lnk_firma_tel_id);


--
-- Name: lnk_firma_telefon lnk_firma_telefon_uni; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_firma_telefon
    ADD CONSTRAINT lnk_firma_telefon_uni UNIQUE (firma_id, telefon, validity);


--
-- Name: lnk_firma_telefon lnk_firma_telefon_validity_excl; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_firma_telefon
    ADD CONSTRAINT lnk_firma_telefon_validity_excl EXCLUDE USING gist (firma_id WITH =, telefon WITH =, validity WITH &&);


--
-- Name: lnk_person_gruppe lnk_person_gruppe_pkey; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_person_gruppe
    ADD CONSTRAINT lnk_person_gruppe_pkey PRIMARY KEY (lnk_person_gruppe_id);


--
-- Name: lnk_person_gruppe lnk_person_gruppe_uni; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_person_gruppe
    ADD CONSTRAINT lnk_person_gruppe_uni UNIQUE (person_id, gruppen_id);


--
-- Name: lnk_person_ort lnk_person_ort_pkey; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_person_ort
    ADD CONSTRAINT lnk_person_ort_pkey PRIMARY KEY (lnk_per_ort_id);


--
-- Name: lnk_person_telefon lnk_person_telefon_pkey; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_person_telefon
    ADD CONSTRAINT lnk_person_telefon_pkey PRIMARY KEY (lnk_person_tel_id);


--
-- Name: lnk_person_telefon lnk_person_telefon_uni; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_person_telefon
    ADD CONSTRAINT lnk_person_telefon_uni UNIQUE (person_id, telefon, validity);


--
-- Name: lnk_person_telefon lnk_person_telefon_validity_excl; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_person_telefon
    ADD CONSTRAINT lnk_person_telefon_validity_excl EXCLUDE USING gist (person_id WITH =, telefon WITH =, validity WITH &&);


--
-- Name: sal_firma_ort sal_firma_ort_pk; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.sal_firma_ort
    ADD CONSTRAINT sal_firma_ort_pk PRIMARY KEY (lnk_fir_ort_id, validity);


--
-- Name: sal_person_ort sal_person_ort_pkey; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.sal_person_ort
    ADD CONSTRAINT sal_person_ort_pkey PRIMARY KEY (lnk_per_ort_id, validity);


--
-- Name: sal_person_ort sal_person_ort_validity_excl; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.sal_person_ort
    ADD CONSTRAINT sal_person_ort_validity_excl EXCLUDE USING gist (lnk_per_ort_id WITH =, ort_is_home WITH =, validity WITH &&);


--
-- Name: sat_firma_epost sat_firma_epost_uni; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.sat_firma_epost
    ADD CONSTRAINT sat_firma_epost_uni UNIQUE (firma_id, epost, validity);


--
-- Name: sat_firma_epost sat_firma_epost_validity_excl; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.sat_firma_epost
    ADD CONSTRAINT sat_firma_epost_validity_excl EXCLUDE USING gist (firma_id WITH =, epost WITH =, validity WITH &&);


--
-- Name: sat_person_bild sat_person_bild_pkey; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.sat_person_bild
    ADD CONSTRAINT sat_person_bild_pkey PRIMARY KEY (person_id);


--
-- Name: sat_person_epost sat_person_epost_uni; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.sat_person_epost
    ADD CONSTRAINT sat_person_epost_uni UNIQUE (person_id, epost, validity);


--
-- Name: sat_person_epost sat_person_epost_validity_excl; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.sat_person_epost
    ADD CONSTRAINT sat_person_epost_validity_excl EXCLUDE USING gist (person_id WITH =, epost WITH =, validity WITH &&);


--
-- Name: sat_person_namen sat_person_namen_pk; Type: CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.sat_person_namen
    ADD CONSTRAINT sat_person_namen_pk PRIMARY KEY (person_id, namen_position);


--
-- Name: firma_gruppe_idx; Type: INDEX; Schema: adressen; Owner: -
--

CREATE UNIQUE INDEX firma_gruppe_idx ON adressen.lnk_firma_gruppe USING btree (firma_id, gruppen_id);


--
-- Name: firma_ort_idx; Type: INDEX; Schema: adressen; Owner: -
--

CREATE UNIQUE INDEX firma_ort_idx ON adressen.lnk_firma_ort USING btree (firma_id, ort_id);


--
-- Name: firma_telefon_idx; Type: INDEX; Schema: adressen; Owner: -
--

CREATE UNIQUE INDEX firma_telefon_idx ON adressen.lnk_firma_telefon USING btree (firma_id, telefon);


--
-- Name: gruppen_name; Type: INDEX; Schema: adressen; Owner: -
--

CREATE UNIQUE INDEX gruppen_name ON adressen.hub_gruppe USING btree (gruppen_name);


--
-- Name: hub_ort_parent_idx; Type: INDEX; Schema: adressen; Owner: -
--

CREATE INDEX hub_ort_parent_idx ON adressen.hub_ort USING btree (parentort);


--
-- Name: hub_ort_region_idx; Type: INDEX; Schema: adressen; Owner: -
--

CREATE UNIQUE INDEX hub_ort_region_idx ON adressen.hub_ort USING btree (ortsname, iso_3166_1, iso_3166_2);


--
-- Name: person_gruppe_idx; Type: INDEX; Schema: adressen; Owner: -
--

CREATE UNIQUE INDEX person_gruppe_idx ON adressen.lnk_person_gruppe USING btree (person_id, gruppen_id);


--
-- Name: person_ort_idx; Type: INDEX; Schema: adressen; Owner: -
--

CREATE UNIQUE INDEX person_ort_idx ON adressen.lnk_person_ort USING btree (person_id, ort_id);


--
-- Name: person_telefon_idx; Type: INDEX; Schema: adressen; Owner: -
--

CREATE UNIQUE INDEX person_telefon_idx ON adressen.lnk_person_telefon USING btree (person_id, telefon);


--
-- Name: sat_person_bild_idx; Type: INDEX; Schema: adressen; Owner: -
--

CREATE UNIQUE INDEX sat_person_bild_idx ON adressen.sat_person_bild USING btree (person_id);


--
-- Name: hub_ort hub_ort_iso_3166_1_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.hub_ort
    ADD CONSTRAINT hub_ort_iso_3166_1_fkey FOREIGN KEY (iso_3166_1, iso_3166_2) REFERENCES adressen.hub_region(iso_3166_1, iso_3166_2);


--
-- Name: hub_ort hub_ort_parentort_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.hub_ort
    ADD CONSTRAINT hub_ort_parentort_fkey FOREIGN KEY (parentort) REFERENCES adressen.hub_ort(ort_id);


--
-- Name: hub_region hub_region_iso_3166_1_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.hub_region
    ADD CONSTRAINT hub_region_iso_3166_1_fkey FOREIGN KEY (iso_3166_1) REFERENCES adressen.hub_land(iso_3166_1);


--
-- Name: hub_telefon hub_telefon_hub_telefon_typ_fk; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.hub_telefon
    ADD CONSTRAINT hub_telefon_hub_telefon_typ_fk FOREIGN KEY (tel_typ) REFERENCES adressen.hub_telefon_typ(tel_typ);


--
-- Name: lnk_firma_gruppe lnk_firma_gruppe_firma_id_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_firma_gruppe
    ADD CONSTRAINT lnk_firma_gruppe_firma_id_fkey FOREIGN KEY (firma_id) REFERENCES adressen.hub_firma(firma_id);


--
-- Name: lnk_firma_gruppe lnk_firma_gruppe_gruppen_id_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_firma_gruppe
    ADD CONSTRAINT lnk_firma_gruppe_gruppen_id_fkey FOREIGN KEY (gruppen_id) REFERENCES adressen.hub_gruppe(gruppen_id);


--
-- Name: lnk_firma_ort lnk_firma_ort_firma_id_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_firma_ort
    ADD CONSTRAINT lnk_firma_ort_firma_id_fkey FOREIGN KEY (firma_id) REFERENCES adressen.hub_firma(firma_id);


--
-- Name: lnk_firma_ort lnk_firma_ort_ort_id_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_firma_ort
    ADD CONSTRAINT lnk_firma_ort_ort_id_fkey FOREIGN KEY (ort_id) REFERENCES adressen.hub_ort(ort_id);


--
-- Name: lnk_firma_telefon lnk_firma_telefon_firma_id_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_firma_telefon
    ADD CONSTRAINT lnk_firma_telefon_firma_id_fkey FOREIGN KEY (firma_id) REFERENCES adressen.hub_firma(firma_id) DEFERRABLE;


--
-- Name: lnk_firma_telefon lnk_firma_telefon_telefon_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_firma_telefon
    ADD CONSTRAINT lnk_firma_telefon_telefon_fkey FOREIGN KEY (telefon) REFERENCES adressen.hub_telefon(telefon) DEFERRABLE;


--
-- Name: lnk_person_gruppe lnk_person_gruppe_gruppen_id_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_person_gruppe
    ADD CONSTRAINT lnk_person_gruppe_gruppen_id_fkey FOREIGN KEY (gruppen_id) REFERENCES adressen.hub_gruppe(gruppen_id);


--
-- Name: lnk_person_gruppe lnk_person_gruppe_person_id_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_person_gruppe
    ADD CONSTRAINT lnk_person_gruppe_person_id_fkey FOREIGN KEY (person_id) REFERENCES adressen.hub_person(person_id);


--
-- Name: lnk_person_ort lnk_person_ort_ort_id_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_person_ort
    ADD CONSTRAINT lnk_person_ort_ort_id_fkey FOREIGN KEY (ort_id) REFERENCES adressen.hub_ort(ort_id);


--
-- Name: lnk_person_ort lnk_person_ort_person_id_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_person_ort
    ADD CONSTRAINT lnk_person_ort_person_id_fkey FOREIGN KEY (person_id) REFERENCES adressen.hub_person(person_id);


--
-- Name: lnk_person_telefon lnk_person_telefon_person_id_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_person_telefon
    ADD CONSTRAINT lnk_person_telefon_person_id_fkey FOREIGN KEY (person_id) REFERENCES adressen.hub_person(person_id) DEFERRABLE;


--
-- Name: lnk_person_telefon lnk_person_telefon_telefon_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.lnk_person_telefon
    ADD CONSTRAINT lnk_person_telefon_telefon_fkey FOREIGN KEY (telefon) REFERENCES adressen.hub_telefon(telefon) DEFERRABLE;


--
-- Name: sal_firma_ort sal_firma_ort_lnk_fir_ort_id_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.sal_firma_ort
    ADD CONSTRAINT sal_firma_ort_lnk_fir_ort_id_fkey FOREIGN KEY (lnk_fir_ort_id) REFERENCES adressen.lnk_firma_ort(lnk_fir_ort_id);


--
-- Name: sal_person_ort sal_person_ort_lnk_per_ort_id_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.sal_person_ort
    ADD CONSTRAINT sal_person_ort_lnk_per_ort_id_fkey FOREIGN KEY (lnk_per_ort_id) REFERENCES adressen.lnk_person_ort(lnk_per_ort_id);


--
-- Name: sat_firma_epost sat_firma_epost_firma_id_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.sat_firma_epost
    ADD CONSTRAINT sat_firma_epost_firma_id_fkey FOREIGN KEY (firma_id) REFERENCES adressen.hub_firma(firma_id);


--
-- Name: sat_person_bild sat_person_bild_person_id_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.sat_person_bild
    ADD CONSTRAINT sat_person_bild_person_id_fkey FOREIGN KEY (person_id) REFERENCES adressen.hub_person(person_id);


--
-- Name: sat_person_epost sat_person_epost_person_id_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.sat_person_epost
    ADD CONSTRAINT sat_person_epost_person_id_fkey FOREIGN KEY (person_id) REFERENCES adressen.hub_person(person_id);


--
-- Name: sat_person_namen sat_person_namen_person_id_fkey; Type: FK CONSTRAINT; Schema: adressen; Owner: -
--

ALTER TABLE ONLY adressen.sat_person_namen
    ADD CONSTRAINT sat_person_namen_person_id_fkey FOREIGN KEY (person_id) REFERENCES adressen.hub_person(person_id);


--
-- PostgreSQL database dump complete
--

