--
-- PostgreSQL database dump
--

-- Dumped from database version 16.2
-- Dumped by pg_dump version 16.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: hub_region; Type: TABLE DATA; Schema: adressen; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE adressen.hub_region DISABLE TRIGGER ALL;

COPY adressen.hub_region (iso_3166_1, iso_3166_2, regionenname, regionenname2) FROM stdin;
DE	SL	Saarland	\N
BR	AC	Acre	\N
BR	AL	Alagoas	\N
BR	AM	Amazonas	\N
BR	AP	Amapá	\N
BR	BA	Bahia	\N
BR	CE	Ceará	\N
BR	DF	Distrito Federal do Brasil	\N
BR	ES	Espírito Santo	\N
BR	GO	Goiás	\N
BR	MA	Maranhão	\N
BR	MG	Minas Gerais	\N
BR	MS	Mato Grosso do Sul	\N
BR	MT	Mato Grosso	\N
BR	PA	Pará	\N
BR	PB	Paraíba	\N
BR	PE	Pernambuco	\N
BR	PI	Piauí	\N
BR	PR	Paraná	\N
BR	RJ	Rio de Janeiro	\N
BR	RN	Rio Grande do Norte	\N
BR	RO	Rondônia	\N
BR	RR	Roraima	\N
BR	RS	Rio Grande do Sul	\N
BR	SC	Santa Catarina	\N
BR	SE	Sergipe	\N
BR	SP	São Paulo	\N
BR	TO	Tocantins	\N
CH	AG	Aargau	\N
CH	AR	Appenzell Ausserrhoden	\N
CH	AI	Appenzell Innerrhoden	\N
CH	BL	Basel-Landschaft	\N
CH	BE	Bern	\N
CH	GE	Genf	\N
CH	GL	Glarus	\N
CH	GR	Graubünden	\N
CH	JU	Jura	\N
CH	LU	Luzern	\N
CH	NE	Neuenburg	\N
CH	NW	Nidwalden	\N
CH	OW	Obwalden	\N
CH	SH	Schaffhausen	\N
CH	SZ	Schwyz	\N
CH	SO	Solothurn	\N
CH	TG	Thurgau	\N
CH	UR	Uri	\N
CH	VS	Wallis	\N
CH	ZG	Zug	\N
CH	ZH	Zürich	\N
CO	AMA	Amazonas	\N
CO	ANT	Antioquia	\N
CO	ARA	Arauca	\N
CO	ATL	Atlántico	\N
CO	BOL	Bolívar	\N
CO	BOY	Boyacá	\N
CO	CAL	Caldas	\N
CO	CAQ	Caquetá	\N
CO	CAS	Casanare	\N
CO	CAU	Cauca	\N
CO	CES	Cesar	\N
CO	CHO	Chocó	\N
CO	COR	Córdoba	\N
CO	CUN	Cundinamarca	\N
CO	DC	Bogotá DC	\N
CO	GUA	Guainía	\N
CO	GUV	Guaviare	\N
CO	HUI	Huila	\N
CO	LAG	La Guajira	\N
CO	MAG	Magdalena	\N
CO	MET	Meta	\N
CO	NAR	Nariño	\N
CO	NSA	Norte de Santander	\N
CO	PUT	Putumayo	\N
CO	QUI	Quindío	\N
CO	RIS	Risaralda	\N
CO	SAP	San Andrés und Providencia	\N
CO	SAN	Santander	\N
CO	SUC	Sucre	\N
CO	TOL	Tolima	\N
CO	VAC	Valle del Cauca	\N
CO	VAU	Vaupés	\N
CO	VID	Vichada	\N
DE	BW	Baden-Württemberg	\N
DE	BY	Bayern	\N
DE	BE	Berlin	\N
DE	BB	Brandenburg	\N
DE	HB	Bremen	\N
DE	HH	Hamburg	\N
DE	HE	Hessen	\N
DE	MV	Mecklenburg-Vorpommern	\N
DE	NI	Niedersachsen	\N
DE	RP	Rheinland-Pfalz	\N
DE	SN	Sachsen	\N
DE	ST	Sachsen-Anhalt	\N
DE	SH	Schleswig-Holstein	\N
DE	TH	Thüringen	\N
DK	81	Nordjylland	\N
DK	82	Midtjylland	\N
DK	83	Syddanmark	\N
DK	84	Hovedstaden	\N
DK	85	Sjælland	\N
FR	01	Ain	\N
FR	02	Aisne	\N
FR	03	Allier	\N
FR	04	Alpes-de-Haute-Provence	\N
FR	05	Hautes-Alpes	\N
FR	06	Alpes-Maritimes	\N
FR	07	Ardèche	\N
FR	08	Ardennes	\N
FR	09	Ariège	\N
FR	10	Aube	\N
FR	11	Aude	\N
FR	12	Aveyron	\N
FR	13	Bouches-du-Rhône	\N
FR	14	Calvados	\N
FR	15	Cantal	\N
FR	16	Charente	\N
FR	17	Charente-Maritime	\N
FR	18	Cher	\N
FR	19	Corrèze	\N
FR	2A	Corse-du-Sud	\N
FR	2B	Haute-Corse	\N
FR	21	Côte-d’Or	\N
FR	22	Côtes-d’Armor	\N
FR	23	Creuse	\N
FR	24	Dordogne	\N
FR	25	Doubs	\N
FR	26	Drôme	\N
FR	27	Eure	\N
FR	28	Eure-et-Loir	\N
FR	29	Finistère	\N
FR	30	Gard	\N
FR	31	Haute-Garonne	\N
FR	32	Gers	\N
FR	33	Gironde	\N
FR	34	Hérault	\N
FR	35	Ille-et-Vilaine	\N
FR	36	Indre	\N
FR	37	Indre-et-Loire	\N
FR	38	Isère	\N
FR	39	Jura	\N
FR	40	Landes	\N
FR	41	Loir-et-Cher	\N
FR	42	Loire	\N
FR	43	Haute-Loire	\N
FR	44	Loire-Atlantique	\N
FR	45	Loiret	\N
FR	46	Lot	\N
FR	47	Lot-et-Garonne	\N
FR	48	Lozère	\N
FR	49	Maine-et-Loire	\N
FR	50	Manche	\N
FR	51	Marne	\N
FR	52	Haute-Marne	\N
FR	53	Mayenne	\N
FR	54	Meurthe-et-Moselle	\N
FR	55	Meuse	\N
FR	56	Morbihan	\N
FR	57	Moselle	\N
FR	58	Nièvre	\N
FR	59	Nord	\N
FR	60	Oise	\N
FR	61	Orne	\N
FR	62	Pas-de-Calais	\N
FR	63	Puy-de-Dôme	\N
CH	FR	Freiburg	Fribourg
CH	TI	Tessin	Ticino
CH	VD	Waadt	Vaud
DE	NW	Nordrhein-Westfalen	NRW
FR	64	Pyrénées-Atlantiques	\N
FR	65	Hautes-Pyrénées	\N
FR	66	Pyrénées-Orientales	\N
FR	67	Bas-Rhin	\N
FR	68	Haut-Rhin	\N
FR	69	Rhône	\N
FR	69M	Métropole de Lyon	\N
FR	70	Haute-Saône	\N
FR	71	Saône-et-Loire	\N
FR	72	Sarthe	\N
FR	73	Savoie	\N
FR	74	Haute-Savoie	\N
FR	75	Paris	\N
FR	76	Seine-Maritime	\N
FR	77	Seine-et-Marne	\N
FR	78	Yvelines	\N
FR	79	Deux-Sèvres	\N
FR	80	Somme	\N
FR	81	Tarn	\N
FR	82	Tarn-et-Garonne	\N
FR	83	Var	\N
FR	84	Vaucluse	\N
FR	85	Vendée	\N
FR	86	Vienne	\N
FR	87	Haute-Vienne	\N
FR	88	Vosges	\N
FR	89	Yonne	\N
FR	90	Territoire de Belfort	\N
FR	91	Essonne	\N
FR	92	Hauts-de-Seine	\N
FR	93	Seine-Saint-Denis	\N
FR	94	Val-de-Marne	\N
FR	95	Val-d’Oise	\N
GR	I	Attika	\N
GR	D	Epirus	\N
GR	F	Ionische Inseln	\N
GR	M	Kreta	\N
GR	H	Mittelgriechenland	\N
GR	K	Nördliche Ägäis	\N
GR	A	Ostmakedonien und Thrakien	\N
GR	J	Peloponnes	\N
GR	L	Südliche Ägäis	\N
GR	E	Thessalien	\N
GR	G	Westgriechenland	\N
GR	C	Westmakedonien	\N
GR	B	Zentralmakedonien	\N
GR	69	Athos	\N
IT	65	Abruzzen	\N
IT	23	Aostatal	\N
IT	75	Apulien	\N
IT	77	Basilikata	\N
IT	45	Emilia-Romagna	\N
IT	36	Friaul-Julisch Venetien	\N
IT	78	Kalabrien	\N
IT	72	Kampanien	\N
IT	62	Latium	\N
IT	42	Ligurien	\N
IT	57	Marken	\N
IT	67	Molise	\N
IT	21	Piemont	\N
IT	88	Sardinien	\N
IT	82	Sizilien	\N
IT	32	Trentino-Südtirol	\N
IT	55	Umbrien	\N
IT	34	Venetien	\N
ES	C	A Coruña	\N
ES	VI	Araba	\N
ES	AB	Albacete	\N
ES	A	Alacant	\N
ES	AL	Almería	\N
ES	O	Asturies	\N
ES	AV	Ávila	\N
ES	BA	Badajoz	\N
ES	PM	Illes Balears	\N
ES	B	Barcelona	\N
ES	BU	Burgos	\N
ES	CC	Cáceres	\N
ES	CA	Cádiz	\N
ES	CS	Castelló	\N
ES	CR	Ciudad Real	\N
ES	CO	Córdoba	\N
ES	CU	Cuenca	\N
ES	GI	Girona	\N
ES	GR	Granada	\N
ES	GU	Guadalajara	\N
ES	SS	Guipúzcoa	\N
ES	H	Huelva	\N
ES	HU	Osca	\N
ES	J	Jaén	\N
ES	S	Kantabrien	\N
ES	LO	La Rioja	\N
ES	GC	Las Palmas	\N
ES	LE	Llion	\N
ES	L	Lhèida	\N
ES	LU	Lugo	\N
ES	M	Madrid	\N
ES	MA	Málaga	\N
ES	MU	Murcia	\N
ES	NA	Nafarroa	\N
ES	OR	Ourense	\N
ES	P	Palencia	\N
ES	PO	Pontevedra	\N
ES	SA	Salamanca	\N
ES	TF	Santa Cruz de Tenerife	\N
ES	Z	Zaragoza	\N
ES	SG	Segovia	\N
ES	SE	Sevilla	\N
ES	SO	Soria	\N
ES	T	Tarragona	\N
ES	TE	Zaragoza	\N
ES	TO	Toledo	\N
ES	V	València	\N
ES	VA	Valladolid	\N
ES	BI	Bizkaia	\N
ES	ZA	Zamora	\N
US	AL	Alabama	\N
US	AK	Alaska	\N
US	AZ	Arizona	\N
US	AR	Arkansas	\N
US	CA	Kalifornien	\N
US	CO	Colorado	\N
US	CT	Connecticut	\N
US	DE	Delaware	\N
US	DC	District of Columbia	\N
US	FL	Florida	\N
US	GA	Georgia	\N
US	HI	Hawaii	\N
US	ID	Idaho	\N
US	IL	Illinois	\N
US	IN	Indiana	\N
US	IA	Iowa	\N
US	KS	Kansas	\N
US	KY	Kentucky	\N
US	LA	Louisiana	\N
US	ME	Maine	\N
US	MD	Maryland	\N
US	MA	Massachusetts	\N
US	MI	Michigan	\N
US	MN	Minnesota	\N
US	MS	Mississippi	\N
US	MO	Missouri	\N
US	MT	Montana	\N
US	NE	Nebraska	\N
US	NV	Nevada	\N
US	NH	New Hampshire	\N
US	NJ	New Jersey	\N
US	NM	New Mexico	\N
US	NY	New York	\N
US	NC	North Carolina	\N
US	ND	North Dakota	\N
US	OH	Ohio	\N
US	OK	Oklahoma	\N
US	OR	Oregon	\N
US	PA	Pennsylvania	\N
US	RI	Rhode Island	\N
US	SC	South Carolina	\N
US	SD	South Dakota	\N
US	TN	Tennessee	\N
US	TX	Texas	\N
US	UT	Utah	\N
US	VT	Vermont	\N
US	VA	Virginia	\N
US	WA	Washington	\N
US	WV	West Virginia	\N
US	WI	Wisconsin	\N
US	WY	Wyoming	\N
TH	50	Chiang Mai	\N
TN	11	Tunis	\N
GB	BRY	Bromley	\N
GB	TAM	Tameside	\N
GB	WYK	West-Yorkshire	\N
MK	85	Skopje	\N
HR	17	Split-Dalmatien	\N
NL	NH	Nordholland	\N
RU	MOW	Moskau Stadt	\N
PT	02	Beja	\N
PT	03	Braga	\N
PT	04	Bragança	\N
PT	05	Castelo Branco	\N
PT	06	Coimbra	\N
PT	08	Faro	\N
PT	09	Guarda	\N
PT	10	Leiria	\N
PT	11	Lisboa	\N
PT	12	Portalegre	\N
PT	13	Porto	\N
PT	30	Região Autónoma da Madeira	\N
PT	20	Região Autónoma dos Açores	\N
PT	14	Santarém	\N
PT	15	Setúbal	\N
PT	16	Viana do Castelo	\N
PT	17	Vila Real	\N
PT	18	Viseu	\N
PT	07	Évora	\N
PT	01	Aveiro	\N
RS	00	Beograd	\N
RS	14	Borski okrug	\N
RS	11	Braničevski okrug	\N
RS	23	Jablanički okrug	\N
RS	04	Južnobanatski okrug	\N
RS	06	Južnobački okrug	\N
RS	09	Kolubarski okrug	\N
RS	KM	Kosovo-Metohija	\N
RS	25	Kosovski okrug	\N
RS	28	Kosovsko-Mitrovački okrug	\N
RS	29	Kosovsko-Pomoravski okrug	\N
RS	08	Mačvanski okrug	\N
RS	17	Moravički okrug	\N
RS	20	Nišavski okrug	\N
RS	26	Pećki okrug	\N
RS	22	Pirotski okrug	\N
RS	10	Podunavski okrug	\N
RS	13	Pomoravski okrug	\N
RS	27	Prizrenski okrug	\N
RS	24	Pčinjski okrug	\N
RS	19	Rasinski okrug	\N
RS	18	Raški okrug	\N
RS	03	Severnobanatski okrug	\N
RS	01	Severnobački okrug	\N
RS	02	Srednjebanatski okrug	\N
RS	07	Sremski okrug	\N
RS	21	Toplički okrug	\N
RS	VO	Vojvodina	\N
RS	15	Zaječarski okrug	\N
RS	05	Zapadnobački okrug	\N
RS	16	Zlatiborski okrug	\N
RS	12	Šumadijski okrug	\N
IT	52	Toskana	Toscana
IT	25	Lombardei	Lombardia
CH	BS	Basel-Stadt	Basel Stadt
CH	SG	Sankt Gallen	St. Gallen
ME	01	Andrijevica	\N
ME	02	Bar	\N
ME	03	Berane	\N
ME	04	Bijelo Polje	\N
ME	05	Budva	\N
ME	06	Cetinje	\N
ME	07	Danilovgrad	\N
ME	08	Herceg Novi	\N
ME	09	Kolašin	\N
ME	10	Kotor	\N
ME	11	Mojkovac	\N
ME	12	Nikšić	\N
ME	13	Plav	\N
ME	14	Pljevlja	\N
ME	15	Plužine	\N
ME	16	Podgorica	\N
ME	17	Rožaje	\N
ME	18	Šavnik	\N
ME	19	Tivat	\N
ME	20	Ulcinj	\N
ME	21	Žabljak	\N
ME	22	Gusinje	\N
ME	23	Petnjica	\N
ME	24	Tuzi	\N
\.


ALTER TABLE adressen.hub_region ENABLE TRIGGER ALL;

--
-- PostgreSQL database dump complete
--

